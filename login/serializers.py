# encoding:utf-8
"""
@file = serializers
@author = zouju
@create_time = 2023-09-14- 14:55
"""

# encoding:utf-8
"""
@file = serializers
@author = zouju
@create_time = 2023-09-13- 13:19
"""
from rest_framework import serializers

from login.models import ExecLog, LoginLog


class ExecLogModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExecLog
        fields = '__all__'


class LoginLogModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = LoginLog
        fields = '__all__'