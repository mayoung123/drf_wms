# encoding:utf-8
"""
@file = urls
@author = zouju
@create_time = 2023-09-14- 10:27
"""
from django.urls import path
from rest_framework import routers

from login import views, log_views
from login.log_views import LoginLogGenericView, EexcLogGenericView

router = routers.DefaultRouter()
router.register(r'login_log', LoginLogGenericView)
router.register(r'exec_log', EexcLogGenericView)

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='登录'),
    path('get-captcha', views.get_captcha, name='获取验证码'),
    path('login-in/', views.login_in, name='登录成功'),
    path('login-out', views.login_out, name='退出登录'),
    path('web-menu', views.web_menu, name='网页目录'),
    path('home', views.home, name='首页'),
    path('logo', views.home, name='首页'),
    path('echarts', views.echarts, name='图表'),

    path('login-log', log_views.login_log_page),
    path('exec-log', log_views.exec_log_page),
]

urlpatterns += router.urls
