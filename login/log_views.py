# encoding:utf-8
"""
@file = log_views
@author = zouju
@create_time = 2023-09-14- 14:55
"""


# encoding:utf-8
"""
@file = generic_views
@author = zouju
@create_time = 2023-09-13- 13:18

日志视图
"""
from django.shortcuts import render

from common.layuiview.layuiviewsets import ListDestroyModelViewSetLayui,ModelViewSetLayui
from login.models import ExecLog, LoginLog
from login.serializers import ExecLogModelSerializer, LoginLogModelSerializer


def login_log_page(request):
    return render(request, 'log/login_log.html')


def exec_log_page(request):
    return render(request, 'log/exec_log.html')


class LoginLogGenericView(ModelViewSetLayui):  # 登陆日志视图
    queryset = LoginLog.objects.all()
    serializer_class = LoginLogModelSerializer


class EexcLogGenericView(ModelViewSetLayui):  # 操作日志视图
    queryset = ExecLog.objects.all()
    serializer_class = ExecLogModelSerializer


