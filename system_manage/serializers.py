# encoding:utf-8
"""
@file = serializers
@author = zouju
@create_time = 2023-09-12- 15:32
"""
from rest_framework import serializers

from system_manage.models import User, Role, Power, Type, Department


class RoleModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = '__all__'


class UserModelSerializer(serializers.ModelSerializer):
    role_name = serializers.CharField(source='role.name', read_only=True)  # Role需要小写role
    dept_name = serializers.CharField(source='department.name', read_only=True)

    # role = RoleModelSerializer(read_only=True)   #　Role模型所有字段

    class Meta:
        model = User
        # fields = '__all__'  # 使用fields字段来明确需要的模型字段，__all__表示包含所有模型字段
        exclude = ['id_password']  # 使用exclude字段表示：可以排除掉哪些模型字段，与上面的字段不能同时使用
        # read_only_fields = ['id']  #　指明只读字段，即仅用于序列化输出时的字段


class RoleModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = '__all__'


class RoleQueryModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = ['id', 'name']


class TypeModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = '__all__'


class PowerModelSerializer(serializers.ModelSerializer):
    type_cn = serializers.CharField(source='type.power_type', read_only=True)

    # power_type = TypeModelSerializer(read_only=True)

    class Meta:
        model = Power
        # fields = '__all__'
        exclude = ['url']


class DeptModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'


class DeptQueryModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ['id', 'name']


class DeptRoleModelSerializer(serializers.ModelSerializer):
    role_all = RoleModelSerializer(read_only=True)
    dept_all = DeptModelSerializer(read_only=True)

    class Meta:
        model = User
        exclude = ['id_password']

