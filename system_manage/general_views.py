# encoding:utf-8
"""
@file = views
@author = zouju
@create_time = 2023-09-18- 14:11
通过视图
"""
from rest_framework.decorators import api_view

from common.API import res_josn_data
from common.authentication.jwt_auth import jwt_login_required


@api_view(['POST'])
@jwt_login_required
def auth_query(request):
    print('授权用户:', request.user)
    return res_josn_data.success_api('成功')