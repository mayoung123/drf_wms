# encoding:utf-8
"""
@file = department
@author = zouju
@create_time = 2023-09-14- 15:00
"""
from django.shortcuts import render

from common.layuiview.layuiviewsets import ModelViewSetLayui
from system_manage.models import Department
from system_manage.serializers import DeptModelSerializer


def dept_manage_page(request):
    return render(request, 'sys_manage/dept_manage/dept_main.html')


def dept_add_page(request):
    return render(request, 'sys_manage/dept_manage/dept_add.html')


class DeptGenericView(ModelViewSetLayui):
    queryset = Department.objects.all()
    serializer_class = DeptModelSerializer
