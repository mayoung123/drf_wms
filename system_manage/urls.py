# encoding:utf-8
"""
@file = urls
@author = zouju
@create_time = 2023-09-12- 15:07
"""
from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import routers

from system_manage import general_views
from system_manage.views import user, role, power, department
from system_manage.views.department import DeptGenericView
from system_manage.views.power import PowerGenericView
from system_manage.views.role import RoleGenericView
from system_manage.views.user import UserGenericView

from rest_framework import permissions


# # 文档视图
schema_view = get_schema_view(
    # API 信息
    openapi.Info(
        title='DRF API',  # API文档标题
        default_version='V1',  # 版本信息
        description='API 接口文档',  # 描述内容
        terms_of_service='https://test.com',  # 开发团队地址
        contact=openapi.Contact(email='https://test.@163com', url='https://test.com'),  # 联系人信息：邮件、网址
        license=openapi.License(name='Test License'),  # 证书
    ),
    public=True,  # 是否公开
    authentication_classes=[],
    permission_classes=[permissions.AllowAny]   # 设置用户权限

)


router = routers.DefaultRouter()
# get,post, put, delete(单个及多个), enable视图
router.register(r'user', UserGenericView)
router.register(r'dept', DeptGenericView)
router.register(r'role', RoleGenericView)
router.register(r'power', PowerGenericView)
urlpatterns = [
    path('auth-query', general_views.auth_query),
    # 返回json数据，需要token授权
    path('user-role-query', user.user_role_query),
    path('user-dept-query', user.user_dept_query),
    path('user-role-update', user.user_role_update),
    path('user-info-query', user.user_info_query),
    # 后缀为page的视图函数返回html页面
    path('user-setting', user.user_setting_page),
    path('user-password', user.user_password_page),
    path('user-setting-save', user.user_setting_save),
    path('user-password-save', user.user_password_save),

    path('user-manage', user.user_manage_page),
    path('user-add', user.user_add_page),
    path('user-role-edit', user.user_role_edit_page),
    path('password-reset', user.password_reset_page),
    path('admin-password-reset', user.password_reset),   # 管理员密码重置

    # 部门管理
    path('department-manage', department.dept_manage_page),
    path('dept-add', department.dept_add_page),
    # 角色管理
    path('role-manage', role.role_manage_page),
    path('role-add', role.role_add_page),
    path('role-power', role.role_power_page),
    path('role-power-query', role.role_power_query),
    path('role-power-save', role.role_power_save),
    # 权限管理
    path('power-manage', power.power_manage_page),
    path('power-add', power.power_add_page),
    path('power-sub-query', power.power_sub_query),
    # swagger api 文档
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),  # 互动模式
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),  # 文档模式

]

urlpatterns += router.urls


