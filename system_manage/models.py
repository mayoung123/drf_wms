# encoding:utf-8
from django.db import models
from django.db.models import SET
from django.utils import timezone


# Create your models here.
class Department(models.Model):
    objects = None
    name = models.CharField('角色名称', max_length=50, null=True)
    leader = models.CharField('库房负责人', max_length=50, null=True)
    enable = models.IntegerField('是否启用', default=1)
    remark = models.CharField('备注', max_length=255, null=True)
    details = models.CharField('详情', max_length=255, null=True)
    address = models.CharField('地址', max_length=255, null=True)
    sort = models.IntegerField('排序', null=True)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)  # 创建时间
    update_time = models.DateTimeField('更新时间', auto_now=True)  # 更新时间

    class Meta:
        db_table = 'sys_department'
        verbose_name = '库房'
        verbose_name_plural = verbose_name


class Role(models.Model):
    objects = None
    name = models.CharField('角色名称', max_length=50, null=True)
    code = models.CharField('角色标识', max_length=50)
    enable = models.IntegerField('是否启用', default=1)
    remark = models.CharField('备注', max_length=255, null=True)
    details = models.CharField('详情', max_length=255, null=True)
    sort = models.IntegerField('排序', null=True)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)  # 创建时间
    update_time = models.DateTimeField('更新时间', auto_now=True)  # 更新时间

    class Meta:
        db_table = 'sys_role'
        verbose_name = '角色'
        verbose_name_plural = verbose_name


class User(models.Model):
    objects = None
    id_number = models.CharField('用户账号', max_length=30, unique=True)
    id_password = models.CharField('用户密码', max_length=255)
    user_name = models.CharField('用户名称', max_length=30)
    department = models.ForeignKey(Department, null=True, default=None, on_delete=SET(None))
    position = models.CharField('职位', max_length=50, blank=True, null=True)
    role = models.ForeignKey(Role, null=True, default=None, on_delete=SET(None))
    enable = models.IntegerField('启用禁用', default=1)
    email = models.CharField('email', max_length=50, blank=True, null=True)
    create_time = models.DateTimeField('创建时间', default=timezone.now)

    def is_authenticated(self):  # permissions # IsAuthenticated校验时会调用该方法
        return True

    def __str__(self):
        return self.user_name

    class Meta:
        db_table = 'sys_user'
        verbose_name = '用户'
        verbose_name_plural = verbose_name


class Type(models.Model):
    objects = None
    power_type = models.CharField('类型', max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'sys_type'
        verbose_name = '权限类型'
        verbose_name_plural = verbose_name


class Power(models.Model):
    objects = None
    name = models.CharField('权限名称', max_length=50)
    type = models.ForeignKey(Type, null=True, default=None, on_delete=SET(None))
    code = models.CharField('权限标识', max_length=30, null=True)
    url = models.CharField('权限路径', max_length=30, null=True)
    parent_id = models.IntegerField('父类编号', default=0)
    icon = models.CharField('图标', max_length=50, null=True)
    sort = models.IntegerField('排序', null=True)
    enable = models.IntegerField('是否开启', default=1)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)  # 创建时间
    update_time = models.DateTimeField('更新时间', auto_now=True)  # 更新时间

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'sys_power'  # 系统权限
        verbose_name = '系统权限'
        verbose_name_plural = verbose_name


class RolePower(models.Model):
    objects = None
    role = models.ForeignKey(Role, null=True, default=None, on_delete=SET(None))
    power = models.ForeignKey(Power, null=True, default=None, on_delete=SET(None))
    power_type = models.ForeignKey(Type, null=True, default=None, on_delete=SET(None))

    class Meta:
        db_table = 'role_power'
        verbose_name = '角色权限'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.power_type
