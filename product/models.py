from django.db import models


# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=64, null=False, blank=False, default='', verbose_name='名字')
    specification = models.CharField(max_length=128, null=False, blank=False, default='', verbose_name='规格')
    price = models.FloatField(null=False, blank=False, default=0, verbose_name='价格')
    quantity = models.BigIntegerField(null=False, blank=False, default=0, verbose_name='数量')

    class Meta:
        db_table = "t_product"
        verbose_name = '产品管理'
        verbose_name_plural = verbose_name


class Shipments(models.Model):
    fk_product = models.ForeignKey(to='Product',
                                   to_field='id',
                                   related_name='fk_product_to_shipments_set',
                                   related_query_name='fk_product_to_shipments_set',
                                   blank=False, null=True, verbose_name='商品表的外键',
                                   on_delete=models.CASCADE)
    quantity = models.BigIntegerField(null=False, blank=False, default=0, verbose_name='数量')

    class Meta:
        db_table = "t_shipments"
        verbose_name = '出库管理'
        verbose_name_plural = verbose_name
