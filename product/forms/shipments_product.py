#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2024/5/9
# @Author  : suk
# @File    : product.py
# @Software: PyCharm
from django import forms

from product.models import Shipments


class ShipmentsForm(forms.ModelForm):
    class Meta:
        fields = "__all__"
        model = Shipments
