# drf_auth_system

#### 介绍
后端使用django-rest-framework， 前端layui mini, 数据库mysql的管理系统，可以快速新增新页面
建议前往CSDN查询相关说明:这里格式不友好
https://blog.csdn.net/qq_27454363/article/details/133032161?spm=1001.2014.3001.5501

#### 软件架构
软件架构说明
1.后端使用: django  django-rest-framework, rest_framework_simplejwt
2.前端layui mini: 官网 http://layuimini.99php.cn/docs/index.html


#### 安装教程

1.安装第三方依赖包:pip install -r requirements.txt
2.mysql新建数据库drf_auth_system
3.生成迁移文件:python manage.py makemigrations
4.创建表:python manage.py migrate
5.插入数据: 在sql/drf_auth_system_data.sql文件
6.python manage.py runserver 8000

#### 优化说明
https://github.com/zoujunqiang/django_auth_system

在此项目上优化以下操作：

1.权限认证有之前的session认证修改为rest_framework_simplejwt的JWT认证，前端发起请求时需要头部需要携带token信息

2.之前页面代码重复率高，常用的CRUD有FBV(Function-Based Views)模式修改为基于DRF(django-rest-framework)的CBV(Class-Based Views)模式，使代码精简，复用率高


#### 使用说明

一.登陆
1.默认管理员用户密码:admin/admin



2.登陆步骤解析：登陆app基于FBV


```

def login(request):
    if request.method == 'GET':
        return render(request, 'login/login.html')
    if request.method == 'POST':
        username = request.POST.get("username")
        password = request.POST.get("password")
        code = request.POST.get('captcha')
        s_code = request.session.get("code")
        print('验证码:', code, s_code)
 
        request.session["code"] = None
 
        if code != s_code:
            login_log(request, uid=username, is_access=False, desc='验证码错误')
            return res_josn_data.fail_api(msg="验证码错误")
 
        user = User.objects.get(id_number=username)
        if user is None:
            login_log(request, username, is_access=False, desc='用户不存在')
            return res_josn_data.fail_api('用户名不正确')
 
        password_valid = check_password(password, user.id_password)
 
        if password_valid:
            print(user.id_number, user.user_name)
            refresh = TokenObtainPairSerializer.get_token(user)
            data = {"userId": user.id_number, "token": str(refresh.access_token), "refresh": str(refresh),
                    "username": user.user_name}
            request.user = user
            request.session["role_id"] = user.role_id  # 通过session的role_id获取用户对应的菜单及权限
            request.session["user_id"] = user.id_number  # 写入日志时需要用到
 
            add_auth_session(request)  # 权限存入 session['permissions']
            login_log(request, username, is_access=True, desc='登陆成功')
            print('data:', data)
            return res_josn_data.table_success(data=data)
        else:
            login_log(request, username, is_access=False, desc='密码错误')
            return res_josn_data.fail_api(msg="密码错误")
```


1前端携带用户名，密码，验证码发起请求；验证通过后将登陆用户的账号及权限ID存入session;

2调用add_auth_session(),查询出该用户的操作权限存入session

3.使用rest_framework_simplejwt模块下TokenObtainPairSerializer的get_token()方法生成token返回,  登陆不需要携带token, 其他请求要求携带token

4.登陆成功后，前端会发起/web-menu请求，查询登陆用户所拥有的权限返回给前端/templates/login/index.html的iniUrl，前端加载对应的页面

二.首页
首页比较简单，显示一个饼图(各数据库的数据量)



三.系统管理
用户管理，库房管理，角色管理，权限管理

CBV模式, 视图类继承ModelViewSetLayui, ModelViewSetLayui在ModelViewSet上基础上改写，返回结果符合layui数据表格，其他代码基本一样；

ModelViewSetLayui继承以下方法，新增的页面时可以直接使用以下方法, 不满足时可重写对应的方法；

请求动作	请求URL	对应视图类方法
查询	get /user/	list()
新增	post /user/	create()
编辑	put /user/id/	update()
删除	delete /user/id/	destroy()
批量删除	delete /user/multiple_delete/	multiple_delete()
开关按钮	put /user/enable/	enable()

1.用户管理



2.库房管理



3.角色管理



4.角色权限

有页面访问权限，按钮权限等，相关权限内容在权限管理页面添加



四.权限管理

可以添加页面，菜单，按钮，其他等四种类型的权限

注意:添加的内容会自动添加到管理员(role_id默认为1)的权限表中

页面：添加新的页面， 如系统管理，日志管理等

菜单:  添加新的菜单页面， 如用户管理，库房管理等

按钮：添加新的按钮权限，如用户管理下的新增，删除等

其他:  添加其他权限，如用户管理下的启用/禁用开关可以选为该类型



五.日志管理
登陆日志，操作日志

CBV模式, 视图类继承ListDestroyModelViewSetLayui

ListDestroyModelViewSetLayui继承以下方法，新增的页面时可以直接使用以下方法；

请求动作	请求URL	对应视图类方法
查询	get /user/	list()
删除	delete /user/id/	destroy()
批量删除	delete /user/multiple_delete/	multiple_delete()
1.登陆日志



2.操作日志



六.新增页面操作步骤
注意：需管理员用户登陆

1.权限管理点击新增

填写名称，图标志，排序，点击保存



2.刷新页面后，显示如下：

左侧菜单栏最下面已经显示新增页面， 如果需要显示在最上面，调整排序栏对应值即可



3.添加菜单

权限管理点击新增，填写名称，代码，排序，选择菜单，父项等

参数说明：

名称：菜单的显示名称

代码: 点击菜单时，前端会发起相应的请求，如填写的是'/new_menu', 切换页面时会发送get /new_menu请求

类型：选择菜单

父项: 菜单属于哪个页面

排序: 菜单的排序顺序



4.刷新页面后，显示如下:



5.后端对应app下的models.py添加相应的模型类

如下面为库房的模型类Department, 继承Model

定义好模型类，则执行数据库迁移：python manage.py makemigrations

生产数据库表：python manage.py migrate



```

# encoding:utf-8
from django.db import models
from django.db.models import SET
from django.utils import timezone
 
 
# Create your models here.
class Department(models.Model):
    objects = None
    name = models.CharField('角色名称', max_length=50, null=True)
    leader = models.CharField('库房负责人', max_length=50, null=True)
    enable = models.IntegerField('是否启用', default=1)
    remark = models.CharField('备注', max_length=255, null=True)
    details = models.CharField('详情', max_length=255, null=True)
    address = models.CharField('地址', max_length=255, null=True)
    sort = models.IntegerField('排序', null=True)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)  # 创建时间
    update_time = models.DateTimeField('更新时间', auto_now=True)  # 更新时间
 
    class Meta:
        db_table = 'sys_department'
        verbose_name = '库房'
        verbose_name_plural = verbose_name
```


6.创建序列化器

对应app下新建文件serializers.py，继承rest_framework的ModelSerializer



from rest_framework import serializers
 
from system_manage.models import Department
 
 
class DeptModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'
7. 创建视图类DeptGenericView, 继承ModelViewSetLayui, 该类和DRF的ModelViewSet代码一样，只修改了返回的数据，

可以放在app下的views.py下，也可以创建单独的视图文，这里放在views下的department.py




```
# encoding:utf-8
"""
@file = department
@author = zouju
@create_time = 2023-09-14- 15:00
"""
from django.shortcuts import render
 
from common.layuiview.layuiviewsets import ModelViewSetLayui
from system_manage.models import Department
from system_manage.serializers import DeptModelSerializer
 
 
def dept_manage_page(request):
    return render(request, 'sys_manage/dept_manage/dept_main.html')
 
 
def dept_add_page(request):
    return render(request, 'sys_manage/dept_manage/dept_add.html')
 
 
class DeptGenericView(ModelViewSetLayui):
    queryset = Department.objects.all()
    serializer_class = DeptModelSerializer
```


8.设置路由

app下的urls.py加入路由



9.前端页面写好后，就可以实现CRUD等操作

10.返回的json数据格式如下：


```
{
    "msg": "success",
    "code": 0,
    "data": [
        {
            "id": 9,
            "name": "行政部",
            "leader": "fzb",
            "enable": 1,
            "remark": null,
            "details": null,
            "address": null,
            "sort": null,
            "create_time": "2023-09-19T15:12:56.905239",
            "update_time": "2023-09-19T15:12:56.905239",
            "count": 1
        },
        {
            "id": 7,
            "name": "财务部",
            "leader": "sxk",
            "enable": 1,
            "remark": null,
            "details": null,
            "address": null,
            "sort": null,
            "create_time": "2023-09-18T15:16:35.846826",
            "update_time": "2023-09-18T15:16:35.846826",
            "count": 2
        },
        {
            "id": 5,
            "name": "业务部",
            "leader": null,
            "enable": 1,
            "remark": null,
            "details": null,
            "address": null,
            "sort": null,
            "create_time": "2023-09-14T15:31:24.444411",
            "update_time": "2023-09-14T15:31:24.444411",
            "count": 3
        },
        {
            "id": 1,
            "name": "研发部",
            "leader": "zjq",
            "enable": 1,
            "remark": null,
            "details": null,
            "address": null,
            "sort": null,
            "create_time": "2023-09-14T14:31:38",
            "update_time": "2023-09-14T15:21:41.407190",
            "count": 4
        }
    ],
    "count": 4,
    "limit": 10
}
```



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
