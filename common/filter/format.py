# encoding:utf-8
"""
@file = format_data
@author = zouju
@create_time = 2023-09-06- 9:03
"""
import json


def vague_filter(data):
    """
    字段为模糊查询
    将查询参数转换成字典,为空则不加入字典,
    :param data:data中的key值要能对应数据库的字段，你可以重写该方法
    """
    orm_field = ['__gt', '__gte', '__lt', '__lte', '__exact', '__iexact', '__contains', '__icontains',
                 '__startswith', '__istartswith', '__endswith', '__iendswith', '__range', '__isnull', '__in']
    filters = {}
    for k, v in data.items():
        if v not in (None, ''):
            db_field = k + orm_field[7]
            filters[db_field] = v
    return filters
