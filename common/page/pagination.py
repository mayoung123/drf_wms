# encoding:utf-8
"""
@file = page
@author = zouju
@create_time = 2023-09-06- 11:18
"""


from rest_framework.pagination import PageNumberPagination


class MyPageNumberPagination(PageNumberPagination):
    page_size = 10
    max_page_size = 100
    page_size_query_param = 'limit'
    page_query_param = 'page'