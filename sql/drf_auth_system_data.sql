
INSERT INTO `sys_role` VALUES (1, '管理员', 'admin', 1, '系统所有权限', NULL, NULL, '2022-08-16 23:14:27.974460', '2022-08-16 23:14:27.974460');

INSERT INTO `sys_department` VALUES (1, '研发部', 'zjq', 1, NULL, NULL, NULL, NULL, '2023-09-14 14:31:38.000000', '2023-09-14 15:21:41.407190');

INSERT INTO `sys_user` VALUES (1, 'admin', 'pbkdf2_sha256$390000$CQPGUE8Q56VJgZFF1TgbgO$gosbmAx9RhF9MyVbLQbLPiyoWRK7+ChAaa1DcyIZHEE=', 'admin', '管理员', 1, '', '2023-09-14 11:07:14.000000', 1, 1);

INSERT INTO `sys_type` VALUES (1, '目录');
INSERT INTO `sys_type` VALUES (2, '菜单');
INSERT INTO `sys_type` VALUES (3, '按钮');
INSERT INTO `sys_type` VALUES (4, '其他');


INSERT INTO `web_logo` VALUES (1, '首页', 'homeInfo-title', 0, '/home', 1, NULL, 1);
INSERT INTO `web_logo` VALUES (2, '后台管理系统', 'logoInfo-title', 1, '/logo', 2, '/static/images/logo.png', 1);
INSERT INTO `web_logo` VALUES (3, '后台管理系统', 'menuInfo-title', 2, '', 3, 'fa fa-address-book', 1);



INSERT INTO `sys_power` VALUES (1, '系统管理', 1, '', NULL, 0, 'fa fa-gears', 1, 1, '2022-08-21 13:40:46.984340', '2022-08-21 13:40:46.984340');
INSERT INTO `sys_power` VALUES (2, '日志管理', 1, NULL, NULL, 0, 'fa fa-sitemap', 2, 1, '2023-09-13 13:28:52.421054', '2023-09-19 10:35:14.953501');
INSERT INTO `sys_power` VALUES (3, '用户管理', 2, '/user-manage', NULL, 1, 'fa fa-user', 1, 1, '2022-08-21 13:41:42.752289', '2022-08-21 13:41:42.753292');
INSERT INTO `sys_power` VALUES (4, '部门管理', 2, '/department-manage', NULL, 1, 'fa fa-navicon', 2, 1, '2022-08-21 13:42:15.823357', '2023-09-19 10:27:24.647448');
INSERT INTO `sys_power` VALUES (5, '角色管理', 2, '/role-manage', NULL, 1, 'fa fa-user-plus', 3, 1, '2022-08-21 13:42:15.823357', '2022-08-21 13:42:15.823357');
INSERT INTO `sys_power` VALUES (6, '权限管理', 2, '/power-manage', NULL, 1, 'fa fa-key', 4, 1, '2022-08-21 13:42:49.405794', '2022-08-21 13:42:49.405794');
INSERT INTO `sys_power` VALUES (7, '用户新增', 3, 'user:add', NULL, 3, '', 1, 1, '2022-08-21 14:21:40.243014', '2022-08-21 14:21:40.243014');
INSERT INTO `sys_power` VALUES (8, '用户删除', 3, 'user:delete', NULL, 3, '', 2, 1, '2022-08-22 00:02:22.548888', '2022-08-22 00:02:22.548888');
INSERT INTO `sys_power` VALUES (9, '用户编辑', 3, 'user:edit', NULL, 3, '', 3, 1, '2022-08-21 14:21:40.243014', '2022-08-21 14:21:40.243014');
INSERT INTO `sys_power` VALUES (10, '角色更新', 3, 'user:role-update', NULL, 3, '', 4, 1, '2022-08-22 00:03:44.909199', '2022-08-22 00:03:44.909199');
INSERT INTO `sys_power` VALUES (11, '禁用启用', 3, 'user:enable', NULL, 3, '', 5, 1, '2022-08-22 00:15:49.725357', '2022-08-22 00:15:49.725357');
INSERT INTO `sys_power` VALUES (12, '部门新增', 3, 'department:add', NULL, 4, '', 1, 1, '2022-08-21 14:21:40.243014', '2022-08-21 14:21:40.243014');
INSERT INTO `sys_power` VALUES (13, '部门删除', 3, 'department:delete', NULL, 4, '', 2, 1, '2022-08-22 00:02:22.548888', '2022-08-22 00:02:22.548888');
INSERT INTO `sys_power` VALUES (14, '部门编辑', 3, 'department:edit', NULL, 4, '', 3, 1, '2022-08-22 00:03:44.909199', '2022-08-22 00:03:44.909199');
INSERT INTO `sys_power` VALUES (15, '禁用启用', 3, 'department:enable', NULL, 4, '', 4, 1, '2022-08-22 00:15:49.725357', '2022-08-22 00:15:49.725357');
INSERT INTO `sys_power` VALUES (16, '角色新增', 3, 'role:add', NULL, 5, '', 1, 1, '2022-08-22 00:23:52.949521', '2022-08-22 00:23:52.949521');
INSERT INTO `sys_power` VALUES (17, '角色删除', 3, 'role:delete', NULL, 5, '', 2, 1, '2022-08-22 00:24:32.076893', '2022-08-22 00:24:32.076893');
INSERT INTO `sys_power` VALUES (18, '角色编辑', 3, 'role:edit', NULL, 5, '', 3, 1, '2022-08-22 00:25:05.360926', '2022-08-22 00:25:05.361931');
INSERT INTO `sys_power` VALUES (19, '角色权限', 3, 'role:power', NULL, 5, '', 4, 1, '2022-08-22 00:25:05.360926', '2022-08-22 00:25:05.361931');
INSERT INTO `sys_power` VALUES (20, '禁用启用', 3, 'role:enable', NULL, 5, '', 5, 1, '2022-08-22 00:25:31.890194', '2022-08-22 00:25:31.890194');
INSERT INTO `sys_power` VALUES (21, '权限新增', 3, 'power:add', NULL, 6, '', 1, 1, '2022-08-22 00:26:06.919411', '2022-08-22 00:26:06.919411');
INSERT INTO `sys_power` VALUES (22, '权限删除', 3, 'power:delete', NULL, 6, '', 2, 1, '2022-08-22 00:26:37.276269', '2022-08-22 00:26:37.276269');
INSERT INTO `sys_power` VALUES (23, '权限编辑', 3, 'power:edit', NULL, 6, '', 3, 1, '2022-08-22 00:26:37.276269', '2022-08-22 00:26:37.276269');
INSERT INTO `sys_power` VALUES (24, '禁用启用', 3, 'power:enable', NULL, 6, '', 4, 1, '2022-08-22 00:27:07.237696', '2022-08-22 00:27:07.237696');
INSERT INTO `sys_power` VALUES (25, '登陆日志', 2, '/login-log', NULL, 2, 'fa fa-outdent', 1, 1, '2023-09-13 14:13:57.457726', '2023-09-19 10:25:23.570577');
INSERT INTO `sys_power` VALUES (26, '操作日志', 2, '/exec-log', NULL, 2, 'fa fa-indent', 2, 1, '2023-09-13 14:12:44.636779', '2023-09-19 10:25:32.620850');
INSERT INTO `sys_power` VALUES (27, '登陆日志删除', 3, 'login-log:delete', NULL, 25, '', 1, 1, '2022-08-24 11:33:45.484369', '2022-08-24 11:33:45.484369');
INSERT INTO `sys_power` VALUES (28, '操作日志删除', 3, 'exec-log:delete', NULL, 26, '', 1, 1, '2022-08-24 11:33:45.484369', '2022-08-24 11:33:45.484369');
INSERT INTO `sys_power` VALUES (38, '重置密码', 2, 'user:reset-password', NULL, 3, NULL, 6, 1, '2023-09-15 16:24:38.406954', '2023-09-15 16:24:58.057427');

INSERT INTO `role_power` VALUES (1, 1, 1, 1);
INSERT INTO `role_power` VALUES (2, 2, 1, 1);
INSERT INTO `role_power` VALUES (3, 3, 2, 1);
INSERT INTO `role_power` VALUES (4, 4, 2, 1);
INSERT INTO `role_power` VALUES (5, 5, 2, 1);
INSERT INTO `role_power` VALUES (6, 6, 2, 1);
INSERT INTO `role_power` VALUES (7, 7, 3, 1);
INSERT INTO `role_power` VALUES (8, 8, 3, 1);
INSERT INTO `role_power` VALUES (9, 9, 3, 1);
INSERT INTO `role_power` VALUES (10, 10, 3, 1);
INSERT INTO `role_power` VALUES (11, 11, 3, 1);
INSERT INTO `role_power` VALUES (12, 12, 3, 1);
INSERT INTO `role_power` VALUES (13, 13, 3, 1);
INSERT INTO `role_power` VALUES (14, 14, 3, 1);
INSERT INTO `role_power` VALUES (15, 15, 3, 1);
INSERT INTO `role_power` VALUES (16, 16, 3, 1);
INSERT INTO `role_power` VALUES (17, 17, 3, 1);
INSERT INTO `role_power` VALUES (18, 18, 3, 1);
INSERT INTO `role_power` VALUES (19, 19, 3, 1);
INSERT INTO `role_power` VALUES (20, 20, 3, 1);
INSERT INTO `role_power` VALUES (21, 21, 3, 1);
INSERT INTO `role_power` VALUES (22, 22, 3, 1);
INSERT INTO `role_power` VALUES (23, 23, 3, 1);
INSERT INTO `role_power` VALUES (24, 24, 3, 1);
INSERT INTO `role_power` VALUES (25, 25, 2, 1);
INSERT INTO `role_power` VALUES (26, 26, 2, 1);
INSERT INTO `role_power` VALUES (27, 27, 3, 1);
INSERT INTO `role_power` VALUES (28, 28, 3, 1);
INSERT INTO `role_power` VALUES (32, 38, 2, 1);












