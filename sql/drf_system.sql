/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80036
 Source Host           : localhost:3306
 Source Schema         : drf_system

 Target Server Type    : MySQL
 Target Server Version : 80036
 File Encoding         : 65001

 Date: 13/05/2024 20:37:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_group_permissions_group_id_permission_id_0cd325b0_uniq`(`group_id` ASC, `permission_id` ASC) USING BTREE,
  INDEX `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm`(`permission_id` ASC) USING BTREE,
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_permission_content_type_id_codename_01ab375a_uniq`(`content_type_id` ASC, `codename` ASC) USING BTREE,
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 97 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO `auth_permission` VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO `auth_permission` VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO `auth_permission` VALUES (4, 'Can view log entry', 1, 'view_logentry');
INSERT INTO `auth_permission` VALUES (5, 'Can add permission', 2, 'add_permission');
INSERT INTO `auth_permission` VALUES (6, 'Can change permission', 2, 'change_permission');
INSERT INTO `auth_permission` VALUES (7, 'Can delete permission', 2, 'delete_permission');
INSERT INTO `auth_permission` VALUES (8, 'Can view permission', 2, 'view_permission');
INSERT INTO `auth_permission` VALUES (9, 'Can add group', 3, 'add_group');
INSERT INTO `auth_permission` VALUES (10, 'Can change group', 3, 'change_group');
INSERT INTO `auth_permission` VALUES (11, 'Can delete group', 3, 'delete_group');
INSERT INTO `auth_permission` VALUES (12, 'Can view group', 3, 'view_group');
INSERT INTO `auth_permission` VALUES (13, 'Can add user', 4, 'add_user');
INSERT INTO `auth_permission` VALUES (14, 'Can change user', 4, 'change_user');
INSERT INTO `auth_permission` VALUES (15, 'Can delete user', 4, 'delete_user');
INSERT INTO `auth_permission` VALUES (16, 'Can view user', 4, 'view_user');
INSERT INTO `auth_permission` VALUES (17, 'Can add content type', 5, 'add_contenttype');
INSERT INTO `auth_permission` VALUES (18, 'Can change content type', 5, 'change_contenttype');
INSERT INTO `auth_permission` VALUES (19, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO `auth_permission` VALUES (20, 'Can view content type', 5, 'view_contenttype');
INSERT INTO `auth_permission` VALUES (21, 'Can add session', 6, 'add_session');
INSERT INTO `auth_permission` VALUES (22, 'Can change session', 6, 'change_session');
INSERT INTO `auth_permission` VALUES (23, 'Can delete session', 6, 'delete_session');
INSERT INTO `auth_permission` VALUES (24, 'Can view session', 6, 'view_session');
INSERT INTO `auth_permission` VALUES (25, 'Can add 系统权限', 7, 'add_power');
INSERT INTO `auth_permission` VALUES (26, 'Can change 系统权限', 7, 'change_power');
INSERT INTO `auth_permission` VALUES (27, 'Can delete 系统权限', 7, 'delete_power');
INSERT INTO `auth_permission` VALUES (28, 'Can view 系统权限', 7, 'view_power');
INSERT INTO `auth_permission` VALUES (29, 'Can add 角色', 8, 'add_role');
INSERT INTO `auth_permission` VALUES (30, 'Can change 角色', 8, 'change_role');
INSERT INTO `auth_permission` VALUES (31, 'Can delete 角色', 8, 'delete_role');
INSERT INTO `auth_permission` VALUES (32, 'Can view 角色', 8, 'view_role');
INSERT INTO `auth_permission` VALUES (33, 'Can add 角色权限', 9, 'add_rolepower');
INSERT INTO `auth_permission` VALUES (34, 'Can change 角色权限', 9, 'change_rolepower');
INSERT INTO `auth_permission` VALUES (35, 'Can delete 角色权限', 9, 'delete_rolepower');
INSERT INTO `auth_permission` VALUES (36, 'Can view 角色权限', 9, 'view_rolepower');
INSERT INTO `auth_permission` VALUES (37, 'Can add 用户', 10, 'add_user');
INSERT INTO `auth_permission` VALUES (38, 'Can change 用户', 10, 'change_user');
INSERT INTO `auth_permission` VALUES (39, 'Can delete 用户', 10, 'delete_user');
INSERT INTO `auth_permission` VALUES (40, 'Can view 用户', 10, 'view_user');
INSERT INTO `auth_permission` VALUES (41, 'Can add 操作日志', 11, 'add_execlog');
INSERT INTO `auth_permission` VALUES (42, 'Can change 操作日志', 11, 'change_execlog');
INSERT INTO `auth_permission` VALUES (43, 'Can delete 操作日志', 11, 'delete_execlog');
INSERT INTO `auth_permission` VALUES (44, 'Can view 操作日志', 11, 'view_execlog');
INSERT INTO `auth_permission` VALUES (45, 'Can add 登陆日志', 12, 'add_loginlog');
INSERT INTO `auth_permission` VALUES (46, 'Can change 登陆日志', 12, 'change_loginlog');
INSERT INTO `auth_permission` VALUES (47, 'Can delete 登陆日志', 12, 'delete_loginlog');
INSERT INTO `auth_permission` VALUES (48, 'Can view 登陆日志', 12, 'view_loginlog');
INSERT INTO `auth_permission` VALUES (49, 'Can add logo信息', 13, 'add_logo');
INSERT INTO `auth_permission` VALUES (50, 'Can change logo信息', 13, 'change_logo');
INSERT INTO `auth_permission` VALUES (51, 'Can delete logo信息', 13, 'delete_logo');
INSERT INTO `auth_permission` VALUES (52, 'Can view logo信息', 13, 'view_logo');
INSERT INTO `auth_permission` VALUES (53, 'Can add 角色权限', 14, 'add_rolepower');
INSERT INTO `auth_permission` VALUES (54, 'Can change 角色权限', 14, 'change_rolepower');
INSERT INTO `auth_permission` VALUES (55, 'Can delete 角色权限', 14, 'delete_rolepower');
INSERT INTO `auth_permission` VALUES (56, 'Can view 角色权限', 14, 'view_rolepower');
INSERT INTO `auth_permission` VALUES (57, 'Can add 用户', 15, 'add_user');
INSERT INTO `auth_permission` VALUES (58, 'Can change 用户', 15, 'change_user');
INSERT INTO `auth_permission` VALUES (59, 'Can delete 用户', 15, 'delete_user');
INSERT INTO `auth_permission` VALUES (60, 'Can view 用户', 15, 'view_user');
INSERT INTO `auth_permission` VALUES (61, 'Can add 权限类型', 16, 'add_powertype');
INSERT INTO `auth_permission` VALUES (62, 'Can change 权限类型', 16, 'change_powertype');
INSERT INTO `auth_permission` VALUES (63, 'Can delete 权限类型', 16, 'delete_powertype');
INSERT INTO `auth_permission` VALUES (64, 'Can view 权限类型', 16, 'view_powertype');
INSERT INTO `auth_permission` VALUES (65, 'Can add 部门', 17, 'add_department');
INSERT INTO `auth_permission` VALUES (66, 'Can change 部门', 17, 'change_department');
INSERT INTO `auth_permission` VALUES (67, 'Can delete 部门', 17, 'delete_department');
INSERT INTO `auth_permission` VALUES (68, 'Can view 部门', 17, 'view_department');
INSERT INTO `auth_permission` VALUES (69, 'Can add 角色', 18, 'add_role');
INSERT INTO `auth_permission` VALUES (70, 'Can change 角色', 18, 'change_role');
INSERT INTO `auth_permission` VALUES (71, 'Can delete 角色', 18, 'delete_role');
INSERT INTO `auth_permission` VALUES (72, 'Can view 角色', 18, 'view_role');
INSERT INTO `auth_permission` VALUES (73, 'Can add 系统权限', 19, 'add_power');
INSERT INTO `auth_permission` VALUES (74, 'Can change 系统权限', 19, 'change_power');
INSERT INTO `auth_permission` VALUES (75, 'Can delete 系统权限', 19, 'delete_power');
INSERT INTO `auth_permission` VALUES (76, 'Can view 系统权限', 19, 'view_power');
INSERT INTO `auth_permission` VALUES (77, 'Can add 权限类型', 16, 'add_type');
INSERT INTO `auth_permission` VALUES (78, 'Can change 权限类型', 16, 'change_type');
INSERT INTO `auth_permission` VALUES (79, 'Can delete 权限类型', 16, 'delete_type');
INSERT INTO `auth_permission` VALUES (80, 'Can view 权限类型', 16, 'view_type');
INSERT INTO `auth_permission` VALUES (81, 'Can add 产品管理', 20, 'add_product');
INSERT INTO `auth_permission` VALUES (82, 'Can change 产品管理', 20, 'change_product');
INSERT INTO `auth_permission` VALUES (83, 'Can delete 产品管理', 20, 'delete_product');
INSERT INTO `auth_permission` VALUES (84, 'Can view 产品管理', 20, 'view_product');
INSERT INTO `auth_permission` VALUES (85, 'Can add shipments', 21, 'add_shipments');
INSERT INTO `auth_permission` VALUES (86, 'Can change shipments', 21, 'change_shipments');
INSERT INTO `auth_permission` VALUES (87, 'Can delete shipments', 21, 'delete_shipments');
INSERT INTO `auth_permission` VALUES (88, 'Can view shipments', 21, 'view_shipments');
INSERT INTO `auth_permission` VALUES (89, 'Can add 产品管理', 22, 'add_product');
INSERT INTO `auth_permission` VALUES (90, 'Can change 产品管理', 22, 'change_product');
INSERT INTO `auth_permission` VALUES (91, 'Can delete 产品管理', 22, 'delete_product');
INSERT INTO `auth_permission` VALUES (92, 'Can view 产品管理', 22, 'view_product');
INSERT INTO `auth_permission` VALUES (93, 'Can add 出库管理', 23, 'add_shipments');
INSERT INTO `auth_permission` VALUES (94, 'Can change 出库管理', 23, 'change_shipments');
INSERT INTO `auth_permission` VALUES (95, 'Can delete 出库管理', 23, 'delete_shipments');
INSERT INTO `auth_permission` VALUES (96, 'Can view 出库管理', 23, 'view_shipments');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_login` datetime(6) NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `first_name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO `auth_user` VALUES (1, 'pbkdf2_sha256$390000$5vA89Tb2eBbPcAnO7DbnFP$ssDcSZo3aXGdM9EOb/1nBITXq5Rtvsf5/JYmQhKtP60=', NULL, 1, 'root', '', '', '', 1, 1, '2023-09-07 09:44:25.356575');

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_user_groups_user_id_group_id_94350c0c_uniq`(`user_id` ASC, `group_id` ASC) USING BTREE,
  INDEX `auth_user_groups_group_id_97559544_fk_auth_group_id`(`group_id` ASC) USING BTREE,
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq`(`user_id` ASC, `permission_id` ASC) USING BTREE,
  INDEX `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm`(`permission_id` ASC) USING BTREE,
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `object_repr` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `action_flag` smallint UNSIGNED NOT NULL,
  `change_message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `content_type_id` int NULL DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `django_admin_log_content_type_id_c4bce8eb_fk_django_co`(`content_type_id` ASC) USING BTREE,
  INDEX `django_admin_log_user_id_c564eba6_fk_auth_user_id`(`user_id` ASC) USING BTREE,
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `model` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `django_content_type_app_label_model_76bd3d3b_uniq`(`app_label` ASC, `model` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES (1, 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES (3, 'auth', 'group');
INSERT INTO `django_content_type` VALUES (2, 'auth', 'permission');
INSERT INTO `django_content_type` VALUES (4, 'auth', 'user');
INSERT INTO `django_content_type` VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES (11, 'login', 'execlog');
INSERT INTO `django_content_type` VALUES (12, 'login', 'loginlog');
INSERT INTO `django_content_type` VALUES (13, 'login', 'logo');
INSERT INTO `django_content_type` VALUES (20, 'product_drf', 'product');
INSERT INTO `django_content_type` VALUES (21, 'product_drf', 'shipments');
INSERT INTO `django_content_type` VALUES (6, 'sessions', 'session');
INSERT INTO `django_content_type` VALUES (17, 'system_manage', 'department');
INSERT INTO `django_content_type` VALUES (19, 'system_manage', 'power');
INSERT INTO `django_content_type` VALUES (18, 'system_manage', 'role');
INSERT INTO `django_content_type` VALUES (14, 'system_manage', 'rolepower');
INSERT INTO `django_content_type` VALUES (16, 'system_manage', 'type');
INSERT INTO `django_content_type` VALUES (15, 'system_manage', 'user');
INSERT INTO `django_content_type` VALUES (7, 'sys_manage', 'power');
INSERT INTO `django_content_type` VALUES (8, 'sys_manage', 'role');
INSERT INTO `django_content_type` VALUES (9, 'sys_manage', 'rolepower');
INSERT INTO `django_content_type` VALUES (10, 'sys_manage', 'user');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `app` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES (1, 'contenttypes', '0001_initial', '2023-09-01 15:23:42.911703');
INSERT INTO `django_migrations` VALUES (2, 'auth', '0001_initial', '2023-09-01 15:23:49.468084');
INSERT INTO `django_migrations` VALUES (3, 'admin', '0001_initial', '2023-09-01 15:23:51.431491');
INSERT INTO `django_migrations` VALUES (4, 'admin', '0002_logentry_remove_auto_add', '2023-09-01 15:23:51.468509');
INSERT INTO `django_migrations` VALUES (5, 'admin', '0003_logentry_add_action_flag_choices', '2023-09-01 15:23:51.506533');
INSERT INTO `django_migrations` VALUES (6, 'contenttypes', '0002_remove_content_type_name', '2023-09-01 15:23:52.334712');
INSERT INTO `django_migrations` VALUES (7, 'auth', '0002_alter_permission_name_max_length', '2023-09-01 15:23:53.147002');
INSERT INTO `django_migrations` VALUES (8, 'auth', '0003_alter_user_email_max_length', '2023-09-01 15:23:53.268539');
INSERT INTO `django_migrations` VALUES (9, 'auth', '0004_alter_user_username_opts', '2023-09-01 15:23:53.305555');
INSERT INTO `django_migrations` VALUES (10, 'auth', '0005_alter_user_last_login_null', '2023-09-01 15:23:53.676914');
INSERT INTO `django_migrations` VALUES (11, 'auth', '0006_require_contenttypes_0002', '2023-09-01 15:23:53.716938');
INSERT INTO `django_migrations` VALUES (12, 'auth', '0007_alter_validators_add_error_messages', '2023-09-01 15:23:53.743934');
INSERT INTO `django_migrations` VALUES (13, 'auth', '0008_alter_user_username_max_length', '2023-09-01 15:23:54.254475');
INSERT INTO `django_migrations` VALUES (14, 'auth', '0009_alter_user_last_name_max_length', '2023-09-01 15:23:54.855255');
INSERT INTO `django_migrations` VALUES (15, 'auth', '0010_alter_group_name_max_length', '2023-09-01 15:23:54.995618');
INSERT INTO `django_migrations` VALUES (16, 'auth', '0011_update_proxy_permissions', '2023-09-01 15:23:55.047679');
INSERT INTO `django_migrations` VALUES (17, 'auth', '0012_alter_user_first_name_max_length', '2023-09-01 15:23:55.548765');
INSERT INTO `django_migrations` VALUES (18, 'sessions', '0001_initial', '2023-09-01 15:23:56.203568');
INSERT INTO `django_migrations` VALUES (19, 'sys_manage', '0001_initial', '2023-09-01 15:23:56.941557');
INSERT INTO `django_migrations` VALUES (20, 'login', '0001_initial', '2023-09-14 10:41:34.215727');
INSERT INTO `django_migrations` VALUES (21, 'system_manage', '0001_initial', '2023-09-14 10:41:38.052855');
INSERT INTO `django_migrations` VALUES (22, 'system_manage', '0002_alter_power_type', '2023-09-14 15:55:15.706153');
INSERT INTO `django_migrations` VALUES (23, 'system_manage', '0003_rename_powertype_type_alter_type_table', '2023-09-14 16:09:28.238953');
INSERT INTO `django_migrations` VALUES (24, 'system_manage', '0004_alter_user_id_number', '2023-09-19 09:22:13.343552');
INSERT INTO `django_migrations` VALUES (25, 'system_manage', '0005_alter_power_parent_id', '2023-09-19 14:03:30.126280');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session`  (
  `session_key` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `session_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`) USING BTREE,
  INDEX `django_session_expire_date_a5c62663`(`expire_date` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO `django_session` VALUES ('16jv4zfegu98dr8j277y1cyquj6v09ow', 'eyJjb2RlIjoiNDIxNSJ9:1qiPzz:pcAxwOuOY1f1FihW94EvhvKGXXDfWdAI1tWq7VmnhuY', '2023-10-03 09:57:59.530388');
INSERT INTO `django_session` VALUES ('9remaulsj6s4n8wk9plv63rfasnocspg', '.eJxtUVtqxDAMvIu-E0J_c5VSgjcSW4NjG8thC6V3rx-y62YXQhjNMKOHv2F3SLDa05gJgjO0aYT1bYKTKRQMCg9tYQIkH5vqKRyaWTvLsL5DUtO3ZM98KKvulMtkUCEeZONA5h5D6d1j9OSEVSE2iGQodoFQx4ZLzulRDbJVN0N1UmksWQPREwdOckemReU2ElJgt5dKjAWXTbrQ_IWVgIp7Qi0lQopmW4y7azunfy7oi3bBnf_LaerlWIGY4uwV88OF1L4-8eKDw3OPWz355ts7_KeN5viCrntcWVnhSqdxMsuf2ueb8rXlk8Bk8aVQp8kLfPz8AmeS8UQ:1s6UrU:KOEQjnJZOKawW1f1EJt5YPfNp6FqAUoA9R-OzYgqndk', '2024-05-27 20:33:00.003137');
INSERT INTO `django_session` VALUES ('axw1s55tm6d2lbnigzh410bm8fhr5lop', '.eJxtjrsKQyEQRP9laiH3Nin8lRBE3CVZ8IWrVci_x1gmNsNyzsLMC6EQw-YRo0ErkZ0Q7NVgKLd1I7A-BQbEtS9yHgaVWxJVKVlhb5j6UluhEbpLPvsHuzpjg6No32BPtKFMsnsmjl86Z9XEuetv5Z9QzrQVa839_QERnF3c:1s64IR:KJh3QIYPBXW2hTI6_RHrUMP_l_L6LVFT5FMxMvAayfA', '2024-05-26 16:11:03.302241');
INSERT INTO `django_session` VALUES ('cv5lg56w6lpnz420s04ly2iq2xcllhv1', '.eJxdkMsKwyAQRf9l1obQkm78lVKKzQxB8IUaUij990ajRroJ9-E9Yj4wWyTgME3XGzDwVtFTIvALgzWQzxoEamn21pHXMgRpTQB-B2BmVYrBmE4OWhix0H5qRHLCR00mdmEid9bZrd8kAheIVSIpiq0glLHqzFkdiq424qWSOy8urC5oxC4r3D6pqHRNgWTZ5tmVYdb5Ja2o-5wWwKEb4bAFUUydjcou0gz7Nxl601x0y09Obf9-lqdAcXAihM16hMf3B27bpWs:1qi7do:g5wy7FDJtaeE20KEk5T0qpRXoIKv9dqkB3cFclZHO14', '2023-10-02 14:21:52.949812');
INSERT INTO `django_session` VALUES ('eslpo2sb5t7o550nbrkjltnedhlx6zm9', 'eyJjb2RlIjoiMTMwMyJ9:1qi27t:oXNiI6wcUqIJowhTI1_St_vsLoOBhEXFsGHogHR58zE', '2023-10-02 08:28:33.189633');
INSERT INTO `django_session` VALUES ('kj9u6fpfuyp34uwm7bwenext2ea3zb8m', 'eyJjb2RlIjoiNjQwOSJ9:1qiUD2:DPP0jIsOgq5X1sHE9m6A_ureX2gCCbp_louldC3r3bc', '2023-10-03 14:27:44.877112');
INSERT INTO `django_session` VALUES ('l1var08iv75zlkljli2mzkjp8j3m0zwl', '.eJxNj8sKwyAQRf9l1ilJuyn4K6UEmxmC4AtNaErpvzea0biR-_Ae8QuTQwJhV607GCPFqJwdafMqfEDcb8PQwRopjApBgESjLHBipaEmC05TvnXtwFMwKqMiiAdAd_D7tLsYaeVM-6JH8jIshuzShInTWO_e7SYRhEQsEknTUgtCtRSdOatH2dRWvnRy58PMaoJKbDLmtklBpWcYkmWdZ8fDrPNPalH2OWXAoSvhsIxgU2a9drOyl_1MhjaaWNf85JS2JM_fHyr5rHM:1qghTV:q3p4EBuOnnvZd0GN_xYzR0ZX2uxygVy_br_tQXVSn5w', '2023-09-14 18:13:21.654505');
INSERT INTO `django_session` VALUES ('pmsr59a0pdfdyqcb85rrwmyhauiv15tf', 'eyJjb2RlIjoiMzIxMCJ9:1qiQ23:Ky7WeqDxvClQlcqLQwi8PFVLrfXovGxOCHQwGWN6TCE', '2023-10-03 10:00:07.629563');
INSERT INTO `django_session` VALUES ('q9ayns68jee8cl8awf91v9n1gas0hejb', '.eJw9yzEKgDAMBdC7_LmDuAi9ikgpNkOgTUtCQRHvLhR1fMO7sNdE8NJzdtCaKXD62I10EDEVFjg00sJmXMXg180hGA0GOhrrCb_M0_ROiYX-ez-wriSc:1qgd3C:6ZpJJImQAHlfU6JpBESykkC0Gcttg7-ea5djanJ_W6k', '2023-09-14 13:29:54.672367');
INSERT INTO `django_session` VALUES ('rs1neyhn4ogvp4hg7pj1vas8pckjn11h', '.eJxdkdsKwyAQRP9lnw2hr_mVUoLNLkXwhhdSKP33RrMa6YvMzDpH1A9sDgkWm7UWEJymVSEsNwE5UqgaJBplQYCnYFSMytkIyx1AnKW57JyMtPJFx64ZycuQDNk0hIU8WO_2sVMIi0RsEklT6gNClZqunOxRDmMrn7q462BmDUEnDhlzx6ShyjEMqbLXq-Ni1fUmfdD6NWXAqTvhtIxg02qzdi9lp2Mtht60se75xWnTv8cKFClNXsa4u4D9jyzt63HFDI_vD9fyq_c:1qiqkW:93MkAIN3WRIr6Gq46rrNH3pvJ43hGKBkE8bzAqW1gVE', '2023-10-04 14:31:48.701241');
INSERT INTO `django_session` VALUES ('vrao91wurxedj0rony4w6fa7xwh4uonb', 'eyJjb2RlIjoiODg1NCJ9:1qiUBT:OGW09ngJoawVldLE2GfSy71myZ_mmfjortM7Z2uBafo', '2023-10-03 14:26:07.357560');

-- ----------------------------
-- Table structure for exec_log
-- ----------------------------
DROP TABLE IF EXISTS `exec_log`;
CREATE TABLE `exec_log`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `uid` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ip` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `success` int NULL DEFAULT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 837 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of exec_log
-- ----------------------------
INSERT INTO `exec_log` VALUES (1, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:30:53.654971');
INSERT INTO `exec_log` VALUES (2, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:30:54.391845');
INSERT INTO `exec_log` VALUES (3, 'PUT', 'admin', '/user/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:48:28.051662');
INSERT INTO `exec_log` VALUES (4, 'PUT', 'admin', '/user/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:48:29.101527');
INSERT INTO `exec_log` VALUES (5, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:53:04.891583');
INSERT INTO `exec_log` VALUES (6, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:53:06.107494');
INSERT INTO `exec_log` VALUES (7, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:53:07.437281');
INSERT INTO `exec_log` VALUES (8, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:53:08.358360');
INSERT INTO `exec_log` VALUES (9, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:53:12.700841');
INSERT INTO `exec_log` VALUES (10, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:53:13.780042');
INSERT INTO `exec_log` VALUES (11, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:54:10.930354');
INSERT INTO `exec_log` VALUES (12, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:54:12.314963');
INSERT INTO `exec_log` VALUES (13, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:54:17.589127');
INSERT INTO `exec_log` VALUES (14, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:54:18.530492');
INSERT INTO `exec_log` VALUES (15, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:54:19.941753');
INSERT INTO `exec_log` VALUES (16, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:54:20.977720');
INSERT INTO `exec_log` VALUES (17, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:58:59.861555');
INSERT INTO `exec_log` VALUES (18, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:59:01.952275');
INSERT INTO `exec_log` VALUES (19, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:59:03.573573');
INSERT INTO `exec_log` VALUES (20, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:59:04.388693');
INSERT INTO `exec_log` VALUES (21, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:59:14.607187');
INSERT INTO `exec_log` VALUES (22, 'GET', 'admin', '/login_log/', '{\'page\': [\'4\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:59:34.007731');
INSERT INTO `exec_log` VALUES (23, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:00:16.412052');
INSERT INTO `exec_log` VALUES (24, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:04:15.781812');
INSERT INTO `exec_log` VALUES (25, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:11:22.767274');
INSERT INTO `exec_log` VALUES (26, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:11:24.526420');
INSERT INTO `exec_log` VALUES (27, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:12:06.979544');
INSERT INTO `exec_log` VALUES (28, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:12:41.193822');
INSERT INTO `exec_log` VALUES (29, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:12:42.064564');
INSERT INTO `exec_log` VALUES (30, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:13:56.926374');
INSERT INTO `exec_log` VALUES (31, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:15:15.578542');
INSERT INTO `exec_log` VALUES (32, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:16:14.401121');
INSERT INTO `exec_log` VALUES (33, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:16:15.981557');
INSERT INTO `exec_log` VALUES (34, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:17:47.095077');
INSERT INTO `exec_log` VALUES (35, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:17:47.976295');
INSERT INTO `exec_log` VALUES (36, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:18:38.537543');
INSERT INTO `exec_log` VALUES (37, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:18:39.397268');
INSERT INTO `exec_log` VALUES (38, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:18:41.199645');
INSERT INTO `exec_log` VALUES (39, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:18:42.435860');
INSERT INTO `exec_log` VALUES (40, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"4\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:18:59.456359');
INSERT INTO `exec_log` VALUES (41, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:19:34.118351');
INSERT INTO `exec_log` VALUES (42, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:19:35.849566');
INSERT INTO `exec_log` VALUES (43, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:19:37.476399');
INSERT INTO `exec_log` VALUES (44, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:19:38.244177');
INSERT INTO `exec_log` VALUES (45, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:21:26.519199');
INSERT INTO `exec_log` VALUES (46, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:21:28.496135');
INSERT INTO `exec_log` VALUES (47, 'PUT', 'admin', '/dept/1/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:21:36.498190');
INSERT INTO `exec_log` VALUES (48, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:21:39.026903');
INSERT INTO `exec_log` VALUES (49, 'PUT', 'admin', '/dept/1/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:21:41.303873');
INSERT INTO `exec_log` VALUES (50, 'PUT', 'admin', '/dept/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:21:44.441736');
INSERT INTO `exec_log` VALUES (51, 'PUT', 'admin', '/dept/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:21:50.388129');
INSERT INTO `exec_log` VALUES (52, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:22:43.885218');
INSERT INTO `exec_log` VALUES (53, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:22:46.117876');
INSERT INTO `exec_log` VALUES (54, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:28:29.141567');
INSERT INTO `exec_log` VALUES (55, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:28:32.040159');
INSERT INTO `exec_log` VALUES (56, 'POST', 'admin', '/dept/dept_add/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:28:44.849726');
INSERT INTO `exec_log` VALUES (57, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:29:20.861651');
INSERT INTO `exec_log` VALUES (58, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:29:23.284584');
INSERT INTO `exec_log` VALUES (59, 'POST', 'admin', '/dept/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:29:32.802170');
INSERT INTO `exec_log` VALUES (60, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:29:35.126371');
INSERT INTO `exec_log` VALUES (61, 'PUT', 'admin', '/dept/3/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:30:18.862892');
INSERT INTO `exec_log` VALUES (62, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:30:25.478605');
INSERT INTO `exec_log` VALUES (63, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:30:27.084214');
INSERT INTO `exec_log` VALUES (64, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:30:37.147735');
INSERT INTO `exec_log` VALUES (65, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:30:40.068978');
INSERT INTO `exec_log` VALUES (66, 'DELETE', 'admin', '/dept/3/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:30:44.617010');
INSERT INTO `exec_log` VALUES (67, 'POST', 'admin', '/dept/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:30:59.553807');
INSERT INTO `exec_log` VALUES (68, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:31:01.862855');
INSERT INTO `exec_log` VALUES (69, 'DELETE', 'admin', '/dept/multiple_delete/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:31:08.412823');
INSERT INTO `exec_log` VALUES (70, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:31:10.782098');
INSERT INTO `exec_log` VALUES (71, 'POST', 'admin', '/dept/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:31:24.407384');
INSERT INTO `exec_log` VALUES (72, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:31:26.709942');
INSERT INTO `exec_log` VALUES (73, 'POST', 'admin', '/dept/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:31:33.711630');
INSERT INTO `exec_log` VALUES (74, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:31:36.101918');
INSERT INTO `exec_log` VALUES (75, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:31:47.608537');
INSERT INTO `exec_log` VALUES (76, 'POST', 'admin', '/role/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:32:16.856055');
INSERT INTO `exec_log` VALUES (77, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:32:19.157878');
INSERT INTO `exec_log` VALUES (78, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:32:21.053235');
INSERT INTO `exec_log` VALUES (79, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:32:22.303470');
INSERT INTO `exec_log` VALUES (80, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"0\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:32:27.362816');
INSERT INTO `exec_log` VALUES (81, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"1\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:32:31.887510');
INSERT INTO `exec_log` VALUES (82, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:32:35.271011');
INSERT INTO `exec_log` VALUES (83, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:33:18.971365');
INSERT INTO `exec_log` VALUES (84, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:38:59.990279');
INSERT INTO `exec_log` VALUES (85, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:42:44.563563');
INSERT INTO `exec_log` VALUES (86, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:56:05.737370');
INSERT INTO `exec_log` VALUES (87, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:58:06.571225');
INSERT INTO `exec_log` VALUES (88, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:59:36.070728');
INSERT INTO `exec_log` VALUES (89, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:59:37.472461');
INSERT INTO `exec_log` VALUES (90, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:59:38.571017');
INSERT INTO `exec_log` VALUES (91, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:00:31.794788');
INSERT INTO `exec_log` VALUES (92, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:03:52.115772');
INSERT INTO `exec_log` VALUES (93, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:04:25.394134');
INSERT INTO `exec_log` VALUES (94, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:05:55.314433');
INSERT INTO `exec_log` VALUES (95, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:06:59.118649');
INSERT INTO `exec_log` VALUES (96, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:10:08.248309');
INSERT INTO `exec_log` VALUES (97, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:11:05.005249');
INSERT INTO `exec_log` VALUES (98, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:11:06.748277');
INSERT INTO `exec_log` VALUES (99, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:11:42.670506');
INSERT INTO `exec_log` VALUES (100, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:11:44.456622');
INSERT INTO `exec_log` VALUES (101, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"1\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:11:47.153145');
INSERT INTO `exec_log` VALUES (102, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:13:28.668173');
INSERT INTO `exec_log` VALUES (103, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:13:29.720768');
INSERT INTO `exec_log` VALUES (104, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:13:35.833384');
INSERT INTO `exec_log` VALUES (105, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:13:36.744726');
INSERT INTO `exec_log` VALUES (106, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"1\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:14:37.923202');
INSERT INTO `exec_log` VALUES (107, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"1\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:14:56.256595');
INSERT INTO `exec_log` VALUES (108, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:18:00.955520');
INSERT INTO `exec_log` VALUES (109, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:18:02.426953');
INSERT INTO `exec_log` VALUES (110, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"1\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:18:28.335796');
INSERT INTO `exec_log` VALUES (111, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"1\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:18:46.766449');
INSERT INTO `exec_log` VALUES (112, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:19:18.667138');
INSERT INTO `exec_log` VALUES (113, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:19:19.530743');
INSERT INTO `exec_log` VALUES (114, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:09:20.789780');
INSERT INTO `exec_log` VALUES (115, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:09:21.919495');
INSERT INTO `exec_log` VALUES (116, 'POST', 'admin', '/user/user_add/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:12:37.275550');
INSERT INTO `exec_log` VALUES (117, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:12:40.031843');
INSERT INTO `exec_log` VALUES (118, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:13:29.431218');
INSERT INTO `exec_log` VALUES (119, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:13:30.254104');
INSERT INTO `exec_log` VALUES (120, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:13:37.573216');
INSERT INTO `exec_log` VALUES (121, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:13:39.519764');
INSERT INTO `exec_log` VALUES (122, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:14:53.387000');
INSERT INTO `exec_log` VALUES (123, 'DELETE', 'admin', '/user/2/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:15:16.084431');
INSERT INTO `exec_log` VALUES (124, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:15:17.857964');
INSERT INTO `exec_log` VALUES (125, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:15:19.061965');
INSERT INTO `exec_log` VALUES (126, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:15:22.530995');
INSERT INTO `exec_log` VALUES (127, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:15:23.472352');
INSERT INTO `exec_log` VALUES (128, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:15:28.285412');
INSERT INTO `exec_log` VALUES (129, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:15:29.423739');
INSERT INTO `exec_log` VALUES (130, 'GET', 'admin', '/power/', '{\'page\': [\'2\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:17:07.057565');
INSERT INTO `exec_log` VALUES (131, 'GET', 'admin', '/power/', '{\'page\': [\'3\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:17:09.048472');
INSERT INTO `exec_log` VALUES (132, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:17:11.845703');
INSERT INTO `exec_log` VALUES (133, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"0\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:17:19.003818');
INSERT INTO `exec_log` VALUES (134, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:17:21.495854');
INSERT INTO `exec_log` VALUES (135, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"fa\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:17:25.763663');
INSERT INTO `exec_log` VALUES (136, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:25:18.546829');
INSERT INTO `exec_log` VALUES (137, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:25:20.539945');
INSERT INTO `exec_log` VALUES (138, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:27:31.952367');
INSERT INTO `exec_log` VALUES (139, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:27:34.463447');
INSERT INTO `exec_log` VALUES (140, 'POST', 'admin', '/user/user_add/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:28:14.314887');
INSERT INTO `exec_log` VALUES (141, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:28:16.989572');
INSERT INTO `exec_log` VALUES (142, 'PUT', 'admin', '/user/3/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:28:38.688018');
INSERT INTO `exec_log` VALUES (143, 'PUT', 'admin', '/user/3/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:28:56.821045');
INSERT INTO `exec_log` VALUES (144, 'POST', 'admin', '/user/user_add/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:29:49.787961');
INSERT INTO `exec_log` VALUES (145, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:29:52.468841');
INSERT INTO `exec_log` VALUES (146, 'POST', 'admin', '/user/user_add/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:30:07.940909');
INSERT INTO `exec_log` VALUES (147, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:30:10.589720');
INSERT INTO `exec_log` VALUES (148, 'POST', 'admin', '/user/user_add/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:30:28.519146');
INSERT INTO `exec_log` VALUES (149, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:30:31.372094');
INSERT INTO `exec_log` VALUES (150, 'PUT', 'admin', '/user/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:30:37.329610');
INSERT INTO `exec_log` VALUES (151, 'DELETE', 'admin', '/user/5/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:30:45.144160');
INSERT INTO `exec_log` VALUES (152, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:30:48.072858');
INSERT INTO `exec_log` VALUES (153, 'DELETE', 'admin', '/user/multiple_delete/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:30:55.112755');
INSERT INTO `exec_log` VALUES (154, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:30:57.480035');
INSERT INTO `exec_log` VALUES (155, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:30:58.760940');
INSERT INTO `exec_log` VALUES (156, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:31:05.846211');
INSERT INTO `exec_log` VALUES (157, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:31:06.717187');
INSERT INTO `exec_log` VALUES (158, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"DELETE\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:31:27.128396');
INSERT INTO `exec_log` VALUES (159, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:31:53.402576');
INSERT INTO `exec_log` VALUES (160, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:31:56.770849');
INSERT INTO `exec_log` VALUES (161, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"员\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:35:24.242989');
INSERT INTO `exec_log` VALUES (162, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:35:27.719997');
INSERT INTO `exec_log` VALUES (163, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:35:36.495704');
INSERT INTO `exec_log` VALUES (164, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:35:38.299009');
INSERT INTO `exec_log` VALUES (165, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"zjq\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:35:43.539654');
INSERT INTO `exec_log` VALUES (166, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:35:46.273143');
INSERT INTO `exec_log` VALUES (167, 'PUT', 'admin', '/dept/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:35:48.314710');
INSERT INTO `exec_log` VALUES (168, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"0\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:35:50.674872');
INSERT INTO `exec_log` VALUES (169, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:35:52.865761');
INSERT INTO `exec_log` VALUES (170, 'PUT', 'admin', '/dept/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:35:53.792209');
INSERT INTO `exec_log` VALUES (171, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"业务\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:36:00.531919');
INSERT INTO `exec_log` VALUES (172, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:36:03.882263');
INSERT INTO `exec_log` VALUES (173, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:36:17.461193');
INSERT INTO `exec_log` VALUES (174, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:36:18.481730');
INSERT INTO `exec_log` VALUES (175, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:36:36.100804');
INSERT INTO `exec_log` VALUES (176, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:14.523340');
INSERT INTO `exec_log` VALUES (177, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:16.630527');
INSERT INTO `exec_log` VALUES (178, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"2\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:19.405888');
INSERT INTO `exec_log` VALUES (179, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"1\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:22.430247');
INSERT INTO `exec_log` VALUES (180, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:27.324644');
INSERT INTO `exec_log` VALUES (181, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"user\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:30.298018');
INSERT INTO `exec_log` VALUES (182, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:32.977567');
INSERT INTO `exec_log` VALUES (183, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"用户\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:37.087250');
INSERT INTO `exec_log` VALUES (184, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"用户\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:39.061007');
INSERT INTO `exec_log` VALUES (185, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:45.821771');
INSERT INTO `exec_log` VALUES (186, 'PUT', 'admin', '/role/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:51.575902');
INSERT INTO `exec_log` VALUES (187, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:54.021221');
INSERT INTO `exec_log` VALUES (188, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"0\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:37:57.702178');
INSERT INTO `exec_log` VALUES (189, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:38:00.780427');
INSERT INTO `exec_log` VALUES (190, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"用户\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:38:10.224534');
INSERT INTO `exec_log` VALUES (191, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"用户\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:38:17.157891');
INSERT INTO `exec_log` VALUES (192, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:38:20.717077');
INSERT INTO `exec_log` VALUES (193, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:38:46.268751');
INSERT INTO `exec_log` VALUES (194, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:38:48.030848');
INSERT INTO `exec_log` VALUES (195, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:38:55.683103');
INSERT INTO `exec_log` VALUES (196, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:38:57.455823');
INSERT INTO `exec_log` VALUES (197, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:39:10.168817');
INSERT INTO `exec_log` VALUES (198, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:39:11.308022');
INSERT INTO `exec_log` VALUES (199, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:41:38.603516');
INSERT INTO `exec_log` VALUES (200, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:41:39.808237');
INSERT INTO `exec_log` VALUES (201, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:41:40.660035');
INSERT INTO `exec_log` VALUES (202, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"1\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:41:45.985568');
INSERT INTO `exec_log` VALUES (203, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:41:48.098628');
INSERT INTO `exec_log` VALUES (204, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"日志\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:49:24.907903');
INSERT INTO `exec_log` VALUES (205, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:49:30.536189');
INSERT INTO `exec_log` VALUES (206, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"log\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:49:33.599433');
INSERT INTO `exec_log` VALUES (207, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"1\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:49:42.222224');
INSERT INTO `exec_log` VALUES (208, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:49:55.211538');
INSERT INTO `exec_log` VALUES (209, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"1\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:49:59.437334');
INSERT INTO `exec_log` VALUES (210, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"2\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:50:02.173119');
INSERT INTO `exec_log` VALUES (211, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"2\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:50:58.000788');
INSERT INTO `exec_log` VALUES (212, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:51:11.619476');
INSERT INTO `exec_log` VALUES (213, 'POST', 'admin', '/power/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:57:46.638152');
INSERT INTO `exec_log` VALUES (214, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 13:28:14.589579');
INSERT INTO `exec_log` VALUES (215, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 13:28:16.463942');
INSERT INTO `exec_log` VALUES (216, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 13:28:17.491417');
INSERT INTO `exec_log` VALUES (217, 'POST', 'admin', '/power/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 13:28:28.594744');
INSERT INTO `exec_log` VALUES (218, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 13:28:30.858535');
INSERT INTO `exec_log` VALUES (219, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 13:32:02.237144');
INSERT INTO `exec_log` VALUES (220, 'POST', 'admin', '/power/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 13:32:14.688302');
INSERT INTO `exec_log` VALUES (221, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 13:40:19.706135');
INSERT INTO `exec_log` VALUES (222, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 13:40:21.985362');
INSERT INTO `exec_log` VALUES (223, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:04:33.016240');
INSERT INTO `exec_log` VALUES (224, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:09:12.200563');
INSERT INTO `exec_log` VALUES (225, 'POST', 'admin', '/power/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:09:55.143516');
INSERT INTO `exec_log` VALUES (226, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:09:57.417042');
INSERT INTO `exec_log` VALUES (227, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"3\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:10:19.147148');
INSERT INTO `exec_log` VALUES (228, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:10:51.291572');
INSERT INTO `exec_log` VALUES (229, 'POST', 'admin', '/power/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:11:02.810784');
INSERT INTO `exec_log` VALUES (230, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:11:05.082707');
INSERT INTO `exec_log` VALUES (231, 'POST', 'admin', '/power/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:14:15.961165');
INSERT INTO `exec_log` VALUES (232, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:14:18.284477');
INSERT INTO `exec_log` VALUES (233, 'POST', 'admin', '/power/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:15:55.889788');
INSERT INTO `exec_log` VALUES (234, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:15:58.179978');
INSERT INTO `exec_log` VALUES (235, 'POST', 'admin', '/power/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:17:03.434992');
INSERT INTO `exec_log` VALUES (236, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:17:05.722842');
INSERT INTO `exec_log` VALUES (237, 'PUT', 'admin', '/power/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:17:35.181491');
INSERT INTO `exec_log` VALUES (238, 'DELETE', 'admin', '/power/multiple_delete/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:17:41.941204');
INSERT INTO `exec_log` VALUES (239, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:17:44.535572');
INSERT INTO `exec_log` VALUES (240, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:17:45.566471');
INSERT INTO `exec_log` VALUES (241, 'POST', 'admin', '/power/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:19:34.987567');
INSERT INTO `exec_log` VALUES (242, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:20:23.066197');
INSERT INTO `exec_log` VALUES (243, 'DELETE', 'admin', '/power/multiple_delete/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:20:28.331591');
INSERT INTO `exec_log` VALUES (244, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:20:30.622160');
INSERT INTO `exec_log` VALUES (245, 'POST', 'admin', '/power/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:21:20.258784');
INSERT INTO `exec_log` VALUES (246, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:21:22.595566');
INSERT INTO `exec_log` VALUES (247, 'PUT', 'admin', '/power/37/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:21:56.378871');
INSERT INTO `exec_log` VALUES (248, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:21:57.901561');
INSERT INTO `exec_log` VALUES (249, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:22:36.917911');
INSERT INTO `exec_log` VALUES (250, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:22:37.688042');
INSERT INTO `exec_log` VALUES (251, 'DELETE', 'admin', '/power/37/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:22:41.708243');
INSERT INTO `exec_log` VALUES (252, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:22:53.277092');
INSERT INTO `exec_log` VALUES (253, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:22:56.241414');
INSERT INTO `exec_log` VALUES (254, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"1\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:23:37.777625');
INSERT INTO `exec_log` VALUES (255, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:23:42.240793');
INSERT INTO `exec_log` VALUES (256, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:23:45.352053');
INSERT INTO `exec_log` VALUES (257, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:23:46.247979');
INSERT INTO `exec_log` VALUES (258, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:23:59.529737');
INSERT INTO `exec_log` VALUES (259, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:24:49.842427');
INSERT INTO `exec_log` VALUES (260, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:24:51.742820');
INSERT INTO `exec_log` VALUES (261, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:24:53.995790');
INSERT INTO `exec_log` VALUES (262, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:25:00.673624');
INSERT INTO `exec_log` VALUES (263, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:25:01.635977');
INSERT INTO `exec_log` VALUES (264, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:25:04.610899');
INSERT INTO `exec_log` VALUES (265, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:25:05.459261');
INSERT INTO `exec_log` VALUES (266, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:26:28.469236');
INSERT INTO `exec_log` VALUES (267, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:26:29.538646');
INSERT INTO `exec_log` VALUES (268, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:26:53.279621');
INSERT INTO `exec_log` VALUES (269, 'GET', 'admin', '/dept/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"leader\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:26:54.394880');
INSERT INTO `exec_log` VALUES (270, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:26:57.230043');
INSERT INTO `exec_log` VALUES (271, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:26:58.566913');
INSERT INTO `exec_log` VALUES (272, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:28:16.967043');
INSERT INTO `exec_log` VALUES (273, 'PUT', 'admin', '/dept/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:28:20.068121');
INSERT INTO `exec_log` VALUES (274, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\",\"role_des\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:28:22.449693');
INSERT INTO `exec_log` VALUES (275, 'PUT', 'admin', '/dept/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:28:39.967819');
INSERT INTO `exec_log` VALUES (276, 'PUT', 'admin', '/role/enable/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:28:42.586448');
INSERT INTO `exec_log` VALUES (277, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:28:43.782897');
INSERT INTO `exec_log` VALUES (278, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:30:32.213532');
INSERT INTO `exec_log` VALUES (279, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:30:35.597343');
INSERT INTO `exec_log` VALUES (280, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:31:18.954003');
INSERT INTO `exec_log` VALUES (281, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:31:19.692812');
INSERT INTO `exec_log` VALUES (282, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:31:20.726135');
INSERT INTO `exec_log` VALUES (283, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:31:29.253665');
INSERT INTO `exec_log` VALUES (284, 'GET', 'admin', '/exec_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:31:32.590318');
INSERT INTO `exec_log` VALUES (285, 'GET', 'admin', '/login_log/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"uid\":\"\",\"method\":\"\",\"url\":\"\",\"desc\":\"\",\"create_time\":\"\",\"status\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:31:36.610021');
INSERT INTO `exec_log` VALUES (286, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:36:30.818171');
INSERT INTO `exec_log` VALUES (287, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:36:31.913379');
INSERT INTO `exec_log` VALUES (288, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:38:59.219627');
INSERT INTO `exec_log` VALUES (289, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:39:00.043905');
INSERT INTO `exec_log` VALUES (290, 'GET', 'admin', '/power/', '{\'page\': [\'2\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:39:04.921042');
INSERT INTO `exec_log` VALUES (291, 'GET', 'admin', '/power/', '{\'page\': [\'3\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:39:07.347904');
INSERT INTO `exec_log` VALUES (292, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:39:13.297823');
INSERT INTO `exec_log` VALUES (293, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:39:14.179554');
INSERT INTO `exec_log` VALUES (294, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:59:42.822440');
INSERT INTO `exec_log` VALUES (295, 'GET', 'admin', '/user/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id_number\":\"\",\"user_name\":\"\",\"department\":\"\",\"position\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:59:43.876227');
INSERT INTO `exec_log` VALUES (296, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:02:44.532320');
INSERT INTO `exec_log` VALUES (297, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:02:45.461181');
INSERT INTO `exec_log` VALUES (298, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:23:33.029048');
INSERT INTO `exec_log` VALUES (299, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:23:33.832084');
INSERT INTO `exec_log` VALUES (300, 'GET', 'admin', '/power/', '{\'page\': [\'3\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:23:38.892704');
INSERT INTO `exec_log` VALUES (301, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"3\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:23:43.434201');
INSERT INTO `exec_log` VALUES (302, 'POST', 'admin', '/power/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:24:38.323921');
INSERT INTO `exec_log` VALUES (303, 'GET', 'admin', '/power/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"name\":\"\",\"code\":\"\",\"type_id\":\"\",\"parent_id\":\"3\",\"icon\":\"\",\"enable\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:24:40.711271');
INSERT INTO `exec_log` VALUES (304, 'PUT', 'admin', '/power/38/', '{}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:24:58.024432');
INSERT INTO `exec_log` VALUES (305, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:25:03.681579');
INSERT INTO `exec_log` VALUES (306, 'GET', 'admin', '/role/', '{\'page\': [\'1\'], \'limit\': [\'10\'], \'Params\': [\'{\"id\":\"\",\"code\":\"\",\"name\":\"\",\"enable\":\"\",\"remark\":\"\"}\']}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:25:05.377038');
INSERT INTO `exec_log` VALUES (508, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:507 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:18.672392');
INSERT INTO `exec_log` VALUES (509, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:506 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:18.827585');
INSERT INTO `exec_log` VALUES (510, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:505 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:18.887584');
INSERT INTO `exec_log` VALUES (511, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:504 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:18.946587');
INSERT INTO `exec_log` VALUES (512, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:503 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.004582');
INSERT INTO `exec_log` VALUES (513, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:502 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.075633');
INSERT INTO `exec_log` VALUES (514, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:501 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.153626');
INSERT INTO `exec_log` VALUES (515, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:500 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.212727');
INSERT INTO `exec_log` VALUES (516, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:499 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.271770');
INSERT INTO `exec_log` VALUES (517, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:498 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.346185');
INSERT INTO `exec_log` VALUES (518, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:497 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.404184');
INSERT INTO `exec_log` VALUES (519, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:496 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.463235');
INSERT INTO `exec_log` VALUES (520, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:494 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.583248');
INSERT INTO `exec_log` VALUES (521, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:493 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.646509');
INSERT INTO `exec_log` VALUES (522, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:492 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.704528');
INSERT INTO `exec_log` VALUES (523, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:491 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.775529');
INSERT INTO `exec_log` VALUES (524, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:490 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.900812');
INSERT INTO `exec_log` VALUES (525, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:489 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:19.958844');
INSERT INTO `exec_log` VALUES (526, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:488 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.016887');
INSERT INTO `exec_log` VALUES (527, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:487 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.074952');
INSERT INTO `exec_log` VALUES (528, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:486 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.133970');
INSERT INTO `exec_log` VALUES (529, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:485 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.191952');
INSERT INTO `exec_log` VALUES (530, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:484 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.250033');
INSERT INTO `exec_log` VALUES (531, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:483 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.309029');
INSERT INTO `exec_log` VALUES (532, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:482 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.388270');
INSERT INTO `exec_log` VALUES (533, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:481 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.446284');
INSERT INTO `exec_log` VALUES (534, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:480 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.504286');
INSERT INTO `exec_log` VALUES (535, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:479 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.563284');
INSERT INTO `exec_log` VALUES (536, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:478 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.621536');
INSERT INTO `exec_log` VALUES (537, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:477 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.679541');
INSERT INTO `exec_log` VALUES (538, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:476 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.737591');
INSERT INTO `exec_log` VALUES (539, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:475 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.796586');
INSERT INTO `exec_log` VALUES (540, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:474 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.866795');
INSERT INTO `exec_log` VALUES (541, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:473 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:20.954794');
INSERT INTO `exec_log` VALUES (542, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:472 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.012818');
INSERT INTO `exec_log` VALUES (543, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:471 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.071843');
INSERT INTO `exec_log` VALUES (544, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:470 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.141913');
INSERT INTO `exec_log` VALUES (545, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:469 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.199960');
INSERT INTO `exec_log` VALUES (546, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:468 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.258967');
INSERT INTO `exec_log` VALUES (547, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:467 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.338245');
INSERT INTO `exec_log` VALUES (548, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:466 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.396320');
INSERT INTO `exec_log` VALUES (549, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:465 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.454331');
INSERT INTO `exec_log` VALUES (550, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:464 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.513420');
INSERT INTO `exec_log` VALUES (551, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:463 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.571420');
INSERT INTO `exec_log` VALUES (552, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:462 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.630140');
INSERT INTO `exec_log` VALUES (553, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:461 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.688139');
INSERT INTO `exec_log` VALUES (554, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:460 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.746209');
INSERT INTO `exec_log` VALUES (555, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:459 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.804208');
INSERT INTO `exec_log` VALUES (556, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:458 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.863465');
INSERT INTO `exec_log` VALUES (557, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:457 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.921459');
INSERT INTO `exec_log` VALUES (558, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:456 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:21.979460');
INSERT INTO `exec_log` VALUES (559, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:455 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.071485');
INSERT INTO `exec_log` VALUES (560, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:454 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.129486');
INSERT INTO `exec_log` VALUES (561, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:453 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.188485');
INSERT INTO `exec_log` VALUES (562, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:452 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.283484');
INSERT INTO `exec_log` VALUES (563, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:451 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.396180');
INSERT INTO `exec_log` VALUES (564, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:450 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.467256');
INSERT INTO `exec_log` VALUES (565, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:449 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.525252');
INSERT INTO `exec_log` VALUES (566, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:448 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.583252');
INSERT INTO `exec_log` VALUES (567, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:447 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.641570');
INSERT INTO `exec_log` VALUES (568, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:446 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.699593');
INSERT INTO `exec_log` VALUES (569, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:445 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.758597');
INSERT INTO `exec_log` VALUES (570, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:444 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.816641');
INSERT INTO `exec_log` VALUES (571, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:443 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.874998');
INSERT INTO `exec_log` VALUES (572, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:442 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.933032');
INSERT INTO `exec_log` VALUES (573, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:441 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:22.992039');
INSERT INTO `exec_log` VALUES (574, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:440 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.050036');
INSERT INTO `exec_log` VALUES (575, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:439 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.128101');
INSERT INTO `exec_log` VALUES (576, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:438 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.188143');
INSERT INTO `exec_log` VALUES (577, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:437 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.246143');
INSERT INTO `exec_log` VALUES (578, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:436 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.305146');
INSERT INTO `exec_log` VALUES (579, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:435 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.363429');
INSERT INTO `exec_log` VALUES (580, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:434 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.421431');
INSERT INTO `exec_log` VALUES (581, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:433 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.480430');
INSERT INTO `exec_log` VALUES (582, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:432 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.538474');
INSERT INTO `exec_log` VALUES (583, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:431 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.596521');
INSERT INTO `exec_log` VALUES (584, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:430 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.654784');
INSERT INTO `exec_log` VALUES (585, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:429 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.712785');
INSERT INTO `exec_log` VALUES (586, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:428 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.771810');
INSERT INTO `exec_log` VALUES (587, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:427 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.862807');
INSERT INTO `exec_log` VALUES (588, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:426 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.921092');
INSERT INTO `exec_log` VALUES (589, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:425 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:23.980086');
INSERT INTO `exec_log` VALUES (590, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:424 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.038092');
INSERT INTO `exec_log` VALUES (591, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:423 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.096087');
INSERT INTO `exec_log` VALUES (592, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:422 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.155091');
INSERT INTO `exec_log` VALUES (593, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:421 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.213088');
INSERT INTO `exec_log` VALUES (594, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:420 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.271108');
INSERT INTO `exec_log` VALUES (595, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:419 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.330135');
INSERT INTO `exec_log` VALUES (596, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:418 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.388420');
INSERT INTO `exec_log` VALUES (597, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:417 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.446418');
INSERT INTO `exec_log` VALUES (598, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:416 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.504465');
INSERT INTO `exec_log` VALUES (599, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:415 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.563464');
INSERT INTO `exec_log` VALUES (600, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:414 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.621471');
INSERT INTO `exec_log` VALUES (601, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:413 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.679696');
INSERT INTO `exec_log` VALUES (602, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:412 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.737721');
INSERT INTO `exec_log` VALUES (603, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:411 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.796753');
INSERT INTO `exec_log` VALUES (604, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:410 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.854753');
INSERT INTO `exec_log` VALUES (605, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:409 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.913421');
INSERT INTO `exec_log` VALUES (606, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:408 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:24.971438');
INSERT INTO `exec_log` VALUES (607, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:407 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:25.030476');
INSERT INTO `exec_log` VALUES (608, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:406 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:51.441208');
INSERT INTO `exec_log` VALUES (609, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:405 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:51.527204');
INSERT INTO `exec_log` VALUES (610, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:404 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:51.604204');
INSERT INTO `exec_log` VALUES (611, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:403 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:51.679309');
INSERT INTO `exec_log` VALUES (612, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:402 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:51.749591');
INSERT INTO `exec_log` VALUES (613, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:401 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:51.812588');
INSERT INTO `exec_log` VALUES (614, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:400 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:51.899636');
INSERT INTO `exec_log` VALUES (615, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:399 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.013937');
INSERT INTO `exec_log` VALUES (616, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:398 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.095930');
INSERT INTO `exec_log` VALUES (617, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:397 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.170927');
INSERT INTO `exec_log` VALUES (618, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:396 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.254384');
INSERT INTO `exec_log` VALUES (619, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:395 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.329386');
INSERT INTO `exec_log` VALUES (620, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:394 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.391429');
INSERT INTO `exec_log` VALUES (621, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:393 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.520423');
INSERT INTO `exec_log` VALUES (622, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:392 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.590472');
INSERT INTO `exec_log` VALUES (623, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:391 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.670557');
INSERT INTO `exec_log` VALUES (624, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:390 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.745869');
INSERT INTO `exec_log` VALUES (625, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:389 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.803872');
INSERT INTO `exec_log` VALUES (626, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:388 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.878870');
INSERT INTO `exec_log` VALUES (627, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:387 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:52.966205');
INSERT INTO `exec_log` VALUES (628, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:386 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:53.032204');
INSERT INTO `exec_log` VALUES (629, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:385 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:53.107227');
INSERT INTO `exec_log` VALUES (630, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:384 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:53.191292');
INSERT INTO `exec_log` VALUES (631, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:383 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:53.257589');
INSERT INTO `exec_log` VALUES (632, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:382 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:53.332627');
INSERT INTO `exec_log` VALUES (633, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:381 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:53.412684');
INSERT INTO `exec_log` VALUES (634, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:380 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:53.591689');
INSERT INTO `exec_log` VALUES (635, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:379 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:53.778007');
INSERT INTO `exec_log` VALUES (636, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:378 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:53.849060');
INSERT INTO `exec_log` VALUES (637, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:377 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:53.916076');
INSERT INTO `exec_log` VALUES (638, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:376 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:53.979425');
INSERT INTO `exec_log` VALUES (639, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:375 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:54.057467');
INSERT INTO `exec_log` VALUES (640, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:374 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:54.157574');
INSERT INTO `exec_log` VALUES (641, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:373 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:54.232613');
INSERT INTO `exec_log` VALUES (642, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:372 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:54.332246');
INSERT INTO `exec_log` VALUES (643, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:371 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:54.426248');
INSERT INTO `exec_log` VALUES (644, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:370 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:54.566310');
INSERT INTO `exec_log` VALUES (645, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:369 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:54.658336');
INSERT INTO `exec_log` VALUES (646, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:368 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:54.733355');
INSERT INTO `exec_log` VALUES (647, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:367 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:54.815737');
INSERT INTO `exec_log` VALUES (648, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:366 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:54.899800');
INSERT INTO `exec_log` VALUES (649, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:365 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:54.982823');
INSERT INTO `exec_log` VALUES (650, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:364 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:55.058145');
INSERT INTO `exec_log` VALUES (651, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:363 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:55.366429');
INSERT INTO `exec_log` VALUES (652, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:362 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:55.460427');
INSERT INTO `exec_log` VALUES (653, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:361 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:55.570514');
INSERT INTO `exec_log` VALUES (654, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:360 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:55.682537');
INSERT INTO `exec_log` VALUES (655, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:359 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:55.765535');
INSERT INTO `exec_log` VALUES (656, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:358 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:55.840769');
INSERT INTO `exec_log` VALUES (657, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:357 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:55.895835');
INSERT INTO `exec_log` VALUES (658, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:356 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:55.978916');
INSERT INTO `exec_log` VALUES (659, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:355 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.046154');
INSERT INTO `exec_log` VALUES (660, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:354 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.116155');
INSERT INTO `exec_log` VALUES (661, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:353 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.191154');
INSERT INTO `exec_log` VALUES (662, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:352 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.266204');
INSERT INTO `exec_log` VALUES (663, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:351 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.349526');
INSERT INTO `exec_log` VALUES (664, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:350 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.420544');
INSERT INTO `exec_log` VALUES (665, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:349 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.527615');
INSERT INTO `exec_log` VALUES (666, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:348 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.637681');
INSERT INTO `exec_log` VALUES (667, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:347 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.754710');
INSERT INTO `exec_log` VALUES (668, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:346 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.820897');
INSERT INTO `exec_log` VALUES (669, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:345 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.890889');
INSERT INTO `exec_log` VALUES (670, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:344 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:56.953890');
INSERT INTO `exec_log` VALUES (671, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:343 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:57.029185');
INSERT INTO `exec_log` VALUES (672, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:342 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:57.112289');
INSERT INTO `exec_log` VALUES (673, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:341 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:57.175314');
INSERT INTO `exec_log` VALUES (674, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:340 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:57.240334');
INSERT INTO `exec_log` VALUES (675, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:339 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:57.304485');
INSERT INTO `exec_log` VALUES (676, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:338 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:57.379558');
INSERT INTO `exec_log` VALUES (677, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:337 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:57.454557');
INSERT INTO `exec_log` VALUES (678, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:336 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:57.707483');
INSERT INTO `exec_log` VALUES (679, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:335 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:57.828818');
INSERT INTO `exec_log` VALUES (680, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:334 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:57.949226');
INSERT INTO `exec_log` VALUES (681, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:333 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:58.041540');
INSERT INTO `exec_log` VALUES (682, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:332 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:58.120564');
INSERT INTO `exec_log` VALUES (683, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:331 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:58.215604');
INSERT INTO `exec_log` VALUES (684, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:330 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:58.323894');
INSERT INTO `exec_log` VALUES (685, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:329 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:58.437934');
INSERT INTO `exec_log` VALUES (686, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:328 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:58.556987');
INSERT INTO `exec_log` VALUES (687, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:327 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:58.682985');
INSERT INTO `exec_log` VALUES (688, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:326 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:58.777980');
INSERT INTO `exec_log` VALUES (689, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:325 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:58.849168');
INSERT INTO `exec_log` VALUES (690, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:324 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:58.924232');
INSERT INTO `exec_log` VALUES (691, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:323 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:58.987264');
INSERT INTO `exec_log` VALUES (692, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:322 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:59.057473');
INSERT INTO `exec_log` VALUES (693, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:321 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:59.124543');
INSERT INTO `exec_log` VALUES (694, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:320 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:59.199544');
INSERT INTO `exec_log` VALUES (695, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:319 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:59.265542');
INSERT INTO `exec_log` VALUES (696, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:318 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:59.337796');
INSERT INTO `exec_log` VALUES (697, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:317 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:59.432878');
INSERT INTO `exec_log` VALUES (698, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:316 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:59.551932');
INSERT INTO `exec_log` VALUES (699, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:315 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:59.654029');
INSERT INTO `exec_log` VALUES (700, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:314 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:59.724046');
INSERT INTO `exec_log` VALUES (701, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:313 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:59.804070');
INSERT INTO `exec_log` VALUES (702, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:312 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:53:59.928775');
INSERT INTO `exec_log` VALUES (703, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:311 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:00.040536');
INSERT INTO `exec_log` VALUES (704, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:310 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:00.112534');
INSERT INTO `exec_log` VALUES (705, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:309 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:00.212608');
INSERT INTO `exec_log` VALUES (706, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:308 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:00.315666');
INSERT INTO `exec_log` VALUES (707, 'DELETE', 'admin', '/exec_log/multiple_delete/', 'id:307 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:00.396031');
INSERT INTO `exec_log` VALUES (708, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:234 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:26.391906');
INSERT INTO `exec_log` VALUES (709, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:233 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:26.473962');
INSERT INTO `exec_log` VALUES (710, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:232 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:26.544963');
INSERT INTO `exec_log` VALUES (711, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:231 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:26.603995');
INSERT INTO `exec_log` VALUES (712, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:230 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:26.661764');
INSERT INTO `exec_log` VALUES (713, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:229 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:26.731780');
INSERT INTO `exec_log` VALUES (714, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:228 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:26.828779');
INSERT INTO `exec_log` VALUES (715, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:227 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:26.914826');
INSERT INTO `exec_log` VALUES (716, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:226 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:26.978840');
INSERT INTO `exec_log` VALUES (717, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:225 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.081943');
INSERT INTO `exec_log` VALUES (718, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:224 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.131939');
INSERT INTO `exec_log` VALUES (719, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:223 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.182145');
INSERT INTO `exec_log` VALUES (720, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:222 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.232158');
INSERT INTO `exec_log` VALUES (721, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:221 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.282194');
INSERT INTO `exec_log` VALUES (722, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:220 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.331527');
INSERT INTO `exec_log` VALUES (723, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:219 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.381517');
INSERT INTO `exec_log` VALUES (724, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:218 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.431516');
INSERT INTO `exec_log` VALUES (725, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:217 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.482520');
INSERT INTO `exec_log` VALUES (726, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:216 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.532562');
INSERT INTO `exec_log` VALUES (727, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:215 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.607279');
INSERT INTO `exec_log` VALUES (728, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:214 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.657281');
INSERT INTO `exec_log` VALUES (729, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:213 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.715612');
INSERT INTO `exec_log` VALUES (730, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:212 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.856641');
INSERT INTO `exec_log` VALUES (731, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:211 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.931633');
INSERT INTO `exec_log` VALUES (732, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:210 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:27.982164');
INSERT INTO `exec_log` VALUES (733, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:209 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.040186');
INSERT INTO `exec_log` VALUES (734, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:208 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.090208');
INSERT INTO `exec_log` VALUES (735, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:207 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.148226');
INSERT INTO `exec_log` VALUES (736, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:206 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.198414');
INSERT INTO `exec_log` VALUES (737, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:205 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.256411');
INSERT INTO `exec_log` VALUES (738, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:204 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.315416');
INSERT INTO `exec_log` VALUES (739, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:203 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.373881');
INSERT INTO `exec_log` VALUES (740, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:202 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.473875');
INSERT INTO `exec_log` VALUES (741, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:201 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.531880');
INSERT INTO `exec_log` VALUES (742, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:200 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.589878');
INSERT INTO `exec_log` VALUES (743, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:199 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.668877');
INSERT INTO `exec_log` VALUES (744, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:198 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.720524');
INSERT INTO `exec_log` VALUES (745, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:197 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.770524');
INSERT INTO `exec_log` VALUES (746, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:196 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.857548');
INSERT INTO `exec_log` VALUES (747, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:195 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:28.914565');
INSERT INTO `exec_log` VALUES (748, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:194 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.039571');
INSERT INTO `exec_log` VALUES (749, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:193 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.095614');
INSERT INTO `exec_log` VALUES (750, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:192 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.145637');
INSERT INTO `exec_log` VALUES (751, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:191 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.194867');
INSERT INTO `exec_log` VALUES (752, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:190 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.244890');
INSERT INTO `exec_log` VALUES (753, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:189 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.294917');
INSERT INTO `exec_log` VALUES (754, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:188 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.345179');
INSERT INTO `exec_log` VALUES (755, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:187 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.395199');
INSERT INTO `exec_log` VALUES (756, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:186 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.445203');
INSERT INTO `exec_log` VALUES (757, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:185 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.495200');
INSERT INTO `exec_log` VALUES (758, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:184 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.545220');
INSERT INTO `exec_log` VALUES (759, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:183 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.595221');
INSERT INTO `exec_log` VALUES (760, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:182 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.645220');
INSERT INTO `exec_log` VALUES (761, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:181 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.695221');
INSERT INTO `exec_log` VALUES (762, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:180 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.744923');
INSERT INTO `exec_log` VALUES (763, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:179 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.794926');
INSERT INTO `exec_log` VALUES (764, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:178 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.914991');
INSERT INTO `exec_log` VALUES (765, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:177 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:29.965016');
INSERT INTO `exec_log` VALUES (766, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:176 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.015029');
INSERT INTO `exec_log` VALUES (767, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:175 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.115029');
INSERT INTO `exec_log` VALUES (768, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:174 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.165074');
INSERT INTO `exec_log` VALUES (769, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:173 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.215292');
INSERT INTO `exec_log` VALUES (770, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:172 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.265345');
INSERT INTO `exec_log` VALUES (771, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:171 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.391672');
INSERT INTO `exec_log` VALUES (772, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:170 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.453724');
INSERT INTO `exec_log` VALUES (773, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:169 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.503750');
INSERT INTO `exec_log` VALUES (774, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:168 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.553755');
INSERT INTO `exec_log` VALUES (775, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:167 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.603751');
INSERT INTO `exec_log` VALUES (776, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:166 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.653752');
INSERT INTO `exec_log` VALUES (777, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:165 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.703761');
INSERT INTO `exec_log` VALUES (778, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:164 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.754002');
INSERT INTO `exec_log` VALUES (779, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:163 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.840025');
INSERT INTO `exec_log` VALUES (780, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:162 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.898042');
INSERT INTO `exec_log` VALUES (781, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:161 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.948071');
INSERT INTO `exec_log` VALUES (782, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:160 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:30.998067');
INSERT INTO `exec_log` VALUES (783, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:159 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.048137');
INSERT INTO `exec_log` VALUES (784, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:158 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.098144');
INSERT INTO `exec_log` VALUES (785, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:157 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.148179');
INSERT INTO `exec_log` VALUES (786, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:156 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.228422');
INSERT INTO `exec_log` VALUES (787, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:155 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.278442');
INSERT INTO `exec_log` VALUES (788, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:154 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.328440');
INSERT INTO `exec_log` VALUES (789, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:153 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.378650');
INSERT INTO `exec_log` VALUES (790, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:152 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.428672');
INSERT INTO `exec_log` VALUES (791, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:151 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.478673');
INSERT INTO `exec_log` VALUES (792, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:150 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.528680');
INSERT INTO `exec_log` VALUES (793, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:149 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.578692');
INSERT INTO `exec_log` VALUES (794, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:148 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.628693');
INSERT INTO `exec_log` VALUES (795, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:147 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.748531');
INSERT INTO `exec_log` VALUES (796, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:146 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.873520');
INSERT INTO `exec_log` VALUES (797, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:145 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.940517');
INSERT INTO `exec_log` VALUES (798, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:144 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:31.990530');
INSERT INTO `exec_log` VALUES (799, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:143 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:32.040516');
INSERT INTO `exec_log` VALUES (800, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:142 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:32.090557');
INSERT INTO `exec_log` VALUES (801, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:141 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:32.140560');
INSERT INTO `exec_log` VALUES (802, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:140 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:32.190557');
INSERT INTO `exec_log` VALUES (803, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:139 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:32.240248');
INSERT INTO `exec_log` VALUES (804, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:138 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:32.290282');
INSERT INTO `exec_log` VALUES (805, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:137 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:32.365594');
INSERT INTO `exec_log` VALUES (806, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:136 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:32.415590');
INSERT INTO `exec_log` VALUES (807, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:135 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:54:32.465598');
INSERT INTO `exec_log` VALUES (808, 'DELETE', 'admin', '/login_log/multiple_delete/', 'id:134 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:55:01.413074');
INSERT INTO `exec_log` VALUES (809, 'DELETE', 'admin', '/user/7/', 'zjq 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:55:33.498249');
INSERT INTO `exec_log` VALUES (810, 'DELETE', 'admin', '/dept/multiple_delete/', 'id:9 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:55:44.514943');
INSERT INTO `exec_log` VALUES (811, 'DELETE', 'admin', '/dept/multiple_delete/', 'id:7 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:55:44.621950');
INSERT INTO `exec_log` VALUES (812, 'DELETE', 'admin', '/dept/multiple_delete/', 'id:5 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:55:44.671946');
INSERT INTO `exec_log` VALUES (813, 'DELETE', 'admin', '/role/multiple_delete/', 'id:3 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:56:06.348053');
INSERT INTO `exec_log` VALUES (814, 'DELETE', 'admin', '/role/multiple_delete/', 'id:2 已删除', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-20 14:56:06.471045');
INSERT INTO `exec_log` VALUES (815, 'POST', 'admin', '/dept/', '{\'id\': 10, \'name\': \'测试部\', \'leader\': \'测试\', \'enable\': 1, \'remark\': None, \'details\': None, \'address\': None, \'sort\': None, \'create_time\': \'2024-05-12T16:07:59.025708\', \'update_time\': \'2024-05-12T16:07:59.025708\'}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:07:59.041607');
INSERT INTO `exec_log` VALUES (816, 'POST', 'admin', '/user/', '{\'id_number\': \'ceshi\', \'id_password\': \'ceshi\', \'user_name\': \'测试\', \'role\': 1, \'department\': \'10\', \'position\': \'\', \'email\': \'\', \'enable\': 1}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:08:15.790829');
INSERT INTO `exec_log` VALUES (817, 'POST', 'admin', '/role/', '{\'id\': 6, \'name\': \'测试\', \'code\': \'测试\', \'enable\': 1, \'remark\': None, \'details\': None, \'sort\': None, \'create_time\': \'2024-05-12T16:08:25.589710\', \'update_time\': \'2024-05-12T16:08:25.589710\'}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:08:25.605245');
INSERT INTO `exec_log` VALUES (818, 'POST', 'admin', '/role-power-save', '测试 权限修改成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:08:36.897362');
INSERT INTO `exec_log` VALUES (819, 'POST', 'admin', '/product_manage_list/', '{\'id\': 1, \'department_name\': \'研发部\', \'name\': \'华为手机\', \'specification\': \'14pro\', \'price\': 1111.0, \'quantity\': 11, \'fk_department\': 1}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:08:54.010106');
INSERT INTO `exec_log` VALUES (820, 'POST', 'admin', '/product_manage_list/', '{\'id\': 2, \'department_name\': \'研发部\', \'name\': \'苹果手机\', \'specification\': \'11\', \'price\': 1111.0, \'quantity\': 1, \'fk_department\': 1}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:09:02.744004');
INSERT INTO `exec_log` VALUES (821, 'POST', 'admin', '/product_manage_list/', '{\'id\': 3, \'department_name\': \'测试部\', \'name\': \'222\', \'specification\': \'222\', \'price\': 222.0, \'quantity\': 222, \'fk_department\': 10}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:09:11.204818');
INSERT INTO `exec_log` VALUES (822, 'POST', 'admin', '/role-power-save', '测试 权限修改成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:10:38.264930');
INSERT INTO `exec_log` VALUES (823, 'POST', 'admin', '/user-role-update', '{\'id\': \'13\', \'id_number\': \'ceshi\', \'role_id\': \'6\'}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:10:47.841449');
INSERT INTO `exec_log` VALUES (824, 'POST', 'admin', '/product_manage_list/', '{\'id\': 1, \'department_name\': \'研发部\', \'name\': \'苹果手机\', \'specification\': \'14pro\', \'price\': 999.0, \'quantity\': 10, \'classification\': \'手机\', \'manage_unit\': \'手机\', \'person\': \'手机\', \'unit\': \'个\', \'user_year\': 0, \'stay_year\': 1, \'quality_grade\': \'新品\', \'other\': \'1\', \'fk_department\': 1}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:56:05.719983');
INSERT INTO `exec_log` VALUES (825, 'POST', 'admin', '/product_manage_list/', '{\'id\': 2, \'department_name\': \'测试部\', \'name\': \'华为手机\', \'specification\': \'mate60\', \'price\': 0.0, \'quantity\': 0, \'classification\': \'手机\', \'manage_unit\': \'手机\', \'person\': \'手机\', \'unit\': \'个\', \'user_year\': 0, \'stay_year\': 1, \'quality_grade\': \'新品\', \'other\': \'1\', \'fk_department\': 10}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:56:23.038149');
INSERT INTO `exec_log` VALUES (826, 'POST', 'ceshi', '/product_manage_list/', '{\'id\': 3, \'department_name\': \'测试部\', \'name\': \'苹果14\', \'specification\': \'1\', \'price\': 1.0, \'quantity\': 1, \'classification\': \'1\', \'manage_unit\': \'1\', \'person\': \'1\', \'unit\': \'1\', \'user_year\': 0, \'stay_year\': 1, \'quality_grade\': \'1\', \'other\': \'1\', \'fk_department\': 10}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:57:45.464090');
INSERT INTO `exec_log` VALUES (827, 'PUT', 'ceshi', '/product_manage_list/2/', '{\'id\': 2, \'department_name\': \'测试部\', \'name\': \'华为手机\', \'specification\': \'mate60\', \'price\': 0.0, \'quantity\': 1, \'classification\': \'手机\', \'manage_unit\': \'手机\', \'person\': \'手机\', \'unit\': \'个\', \'user_year\': 0, \'stay_year\': 1, \'quality_grade\': \'新品\', \'other\': \'1\', \'fk_department\': 10}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:58:05.005155');
INSERT INTO `exec_log` VALUES (828, 'POST', 'admin', '/power/', '{\'name\': \'出库管理\', \'code\': None, \'icon\': None, \'sort\': 1, \'enable\': 1, \'type_id\': 1}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 17:15:21.635359');
INSERT INTO `exec_log` VALUES (829, 'PUT', 'admin', '/power/64/', '{\'id\': 64, \'type_cn\': \'菜单\', \'name\': \'出库管理\', \'code\': \'/shipments_manage_page\', \'parent_id\': 67, \'icon\': \'fa fa-bolt\', \'sort\': 1, \'enable\': 1, \'create_time\': \'2024-05-09T11:16:49.472139\', \'update_time\': \'2024-05-12T17:15:34.970510\', \'type\': 2}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 17:15:34.985573');
INSERT INTO `exec_log` VALUES (830, 'POST', 'admin', '/product_manage_list/', '{\'id\': 4, \'department_name\': \'研发部\', \'name\': \'苹果手机\', \'specification\': \'14pro\', \'price\': 11.0, \'quantity\': 11, \'classification\': \'手机\', \'manage_unit\': \'手机\', \'person\': \'手机\', \'unit\': \'个\', \'user_year\': 0, \'stay_year\': 2, \'quality_grade\': \'新品\', \'other\': \'1\', \'fk_department\': 1}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:16:49.854674');
INSERT INTO `exec_log` VALUES (831, 'PUT', 'admin', '/power/54/', '{\'id\': 54, \'type_cn\': \'目录\', \'name\': \'商品管理\', \'code\': None, \'parent_id\': 0, \'icon\': None, \'sort\': 1, \'enable\': 1, \'create_time\': \'2024-05-08T22:56:45.333217\', \'update_time\': \'2024-05-12T20:31:43.690709\', \'type\': 1}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:31:43.708678');
INSERT INTO `exec_log` VALUES (832, 'PUT', 'admin', '/power/55/', '{\'id\': 55, \'type_cn\': \'菜单\', \'name\': \'商品信息\', \'code\': \'/product_manage_page\', \'parent_id\': 54, \'icon\': \'fa fa-briefcase\', \'sort\': 2, \'enable\': 1, \'create_time\': \'2024-05-08T23:00:47.565114\', \'update_time\': \'2024-05-12T20:31:50.349885\', \'type\': 2}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:31:50.360889');
INSERT INTO `exec_log` VALUES (833, 'PUT', 'admin', '/power/4/', '{\'id\': 4, \'type_cn\': \'菜单\', \'name\': \'库房管理\', \'code\': \'/department-manage\', \'parent_id\': 1, \'icon\': \'fa fa-navicon\', \'sort\': 2, \'enable\': 1, \'create_time\': \'2022-08-21T13:42:15.823357\', \'update_time\': \'2024-05-12T20:32:11.785794\', \'type\': 2}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:32:11.795700');
INSERT INTO `exec_log` VALUES (834, 'POST', 'admin', '/role-power-save', '测试 权限修改成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:35:12.646030');
INSERT INTO `exec_log` VALUES (835, 'PUT', 'ceshi', '/product_manage_list/3/', '{\'id\': 3, \'department_name\': \'测试部\', \'name\': \'苹果14\', \'specification\': \'1\', \'price\': 1.0, \'quantity\': 100, \'classification\': \'1\', \'manage_unit\': \'1\', \'person\': \'1\', \'unit\': \'1\', \'user_year\': 0, \'stay_year\': 1, \'quality_grade\': \'1\', \'other\': \'1\', \'fk_department\': 10}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:35:52.265171');
INSERT INTO `exec_log` VALUES (836, 'PUT', 'admin', '/power/4/', '{\'id\': 4, \'type_cn\': \'菜单\', \'name\': \'部门管理\', \'code\': \'/department-manage\', \'parent_id\': 1, \'icon\': \'fa fa-navicon\', \'sort\': 2, \'enable\': 1, \'create_time\': \'2022-08-21T13:42:15.823357\', \'update_time\': \'2024-05-13T20:33:41.945563\', \'type\': 2}', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-13 20:33:41.954565');

-- ----------------------------
-- Table structure for login_log
-- ----------------------------
DROP TABLE IF EXISTS `login_log`;
CREATE TABLE `login_log`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `uid` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ip` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `success` int NULL DEFAULT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 269 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of login_log
-- ----------------------------
INSERT INTO `login_log` VALUES (1, 'POST', 'admin', '/login', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 11:19:39.287729');
INSERT INTO `login_log` VALUES (2, 'POST', 'admin', '/login', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 11:22:39.936721');
INSERT INTO `login_log` VALUES (3, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 11:29:14.410098');
INSERT INTO `login_log` VALUES (4, 'POST', 'admin', '/login', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 11:29:49.424784');
INSERT INTO `login_log` VALUES (5, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 11:29:54.633368');
INSERT INTO `login_log` VALUES (6, 'POST', 'admin', '/login', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:31:09.142852');
INSERT INTO `login_log` VALUES (7, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:31:12.928972');
INSERT INTO `login_log` VALUES (8, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:32:30.756301');
INSERT INTO `login_log` VALUES (9, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:33:52.671525');
INSERT INTO `login_log` VALUES (10, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:35:01.516080');
INSERT INTO `login_log` VALUES (11, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:37:31.790454');
INSERT INTO `login_log` VALUES (12, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:48:05.571976');
INSERT INTO `login_log` VALUES (13, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:48:59.853463');
INSERT INTO `login_log` VALUES (14, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:49:32.821108');
INSERT INTO `login_log` VALUES (15, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:49:41.220859');
INSERT INTO `login_log` VALUES (16, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:49:48.084717');
INSERT INTO `login_log` VALUES (17, 'POST', 'admin', '/login', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:50:11.077067');
INSERT INTO `login_log` VALUES (18, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:50:20.027268');
INSERT INTO `login_log` VALUES (19, 'POST', 'admin', '/login', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:50:49.893590');
INSERT INTO `login_log` VALUES (20, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:50:55.175565');
INSERT INTO `login_log` VALUES (21, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:51:18.644990');
INSERT INTO `login_log` VALUES (22, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:51:44.587035');
INSERT INTO `login_log` VALUES (23, 'POST', 'admin', '/login', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:53:00.061080');
INSERT INTO `login_log` VALUES (24, 'POST', 'admin', '/login', '登录成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 13:53:03.976231');
INSERT INTO `login_log` VALUES (25, 'POST', 'admin', '/login', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:04:15.269424');
INSERT INTO `login_log` VALUES (26, 'POST', 'admin', '/login', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:04:19.930938');
INSERT INTO `login_log` VALUES (27, 'POST', 'admin', '/login', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:05:55.953237');
INSERT INTO `login_log` VALUES (28, 'POST', 'admin', '/login', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:21:08.942500');
INSERT INTO `login_log` VALUES (29, 'POST', 'admin', '/login', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:25:25.978574');
INSERT INTO `login_log` VALUES (30, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:30:50.605375');
INSERT INTO `login_log` VALUES (31, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:53:01.456206');
INSERT INTO `login_log` VALUES (32, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:53:57.873242');
INSERT INTO `login_log` VALUES (33, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 14:54:01.978013');
INSERT INTO `login_log` VALUES (34, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:07:23.559755');
INSERT INTO `login_log` VALUES (35, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 15:59:31.126593');
INSERT INTO `login_log` VALUES (36, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-14 16:13:21.604505');
INSERT INTO `login_log` VALUES (37, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 08:09:12.789449');
INSERT INTO `login_log` VALUES (38, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 13:28:11.871546');
INSERT INTO `login_log` VALUES (39, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:22:25.618039');
INSERT INTO `login_log` VALUES (40, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:22:30.804401');
INSERT INTO `login_log` VALUES (41, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:25:09.553653');
INSERT INTO `login_log` VALUES (42, 'POST', '100401', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:25:17.677652');
INSERT INTO `login_log` VALUES (43, 'POST', '100401', '/login/', '密码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:25:22.429549');
INSERT INTO `login_log` VALUES (44, 'POST', '100401', '/login/', '密码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:25:34.318748');
INSERT INTO `login_log` VALUES (45, 'POST', '100401', '/login/', '密码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:26:14.692215');
INSERT INTO `login_log` VALUES (46, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:26:25.980282');
INSERT INTO `login_log` VALUES (47, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:35:30.932784');
INSERT INTO `login_log` VALUES (48, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:35:35.676517');
INSERT INTO `login_log` VALUES (49, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:36:16.677146');
INSERT INTO `login_log` VALUES (50, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:37:01.913979');
INSERT INTO `login_log` VALUES (51, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:37:05.789491');
INSERT INTO `login_log` VALUES (52, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:43:22.206490');
INSERT INTO `login_log` VALUES (53, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:46:49.048123');
INSERT INTO `login_log` VALUES (54, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:50:00.954689');
INSERT INTO `login_log` VALUES (55, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:53:32.426361');
INSERT INTO `login_log` VALUES (56, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:59:00.372358');
INSERT INTO `login_log` VALUES (57, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 15:59:38.248463');
INSERT INTO `login_log` VALUES (58, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:05:54.795807');
INSERT INTO `login_log` VALUES (59, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:20:43.545853');
INSERT INTO `login_log` VALUES (60, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:21:51.102204');
INSERT INTO `login_log` VALUES (61, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:22:14.393966');
INSERT INTO `login_log` VALUES (62, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:22:48.625361');
INSERT INTO `login_log` VALUES (63, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:49:24.806255');
INSERT INTO `login_log` VALUES (64, 'POST', '100401', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:49:33.346356');
INSERT INTO `login_log` VALUES (65, 'GET', '100401', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:49:53.270320');
INSERT INTO `login_log` VALUES (66, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:49:58.717029');
INSERT INTO `login_log` VALUES (67, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:50:02.482256');
INSERT INTO `login_log` VALUES (68, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-15 16:50:06.498395');
INSERT INTO `login_log` VALUES (69, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 08:29:03.403979');
INSERT INTO `login_log` VALUES (70, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 08:33:07.048486');
INSERT INTO `login_log` VALUES (71, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 08:40:08.713713');
INSERT INTO `login_log` VALUES (72, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 09:29:08.946335');
INSERT INTO `login_log` VALUES (73, 'POST', '100401', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 09:29:18.525700');
INSERT INTO `login_log` VALUES (74, 'GET', '100401', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 09:29:25.064789');
INSERT INTO `login_log` VALUES (75, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 09:29:36.229571');
INSERT INTO `login_log` VALUES (76, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 14:34:25.570235');
INSERT INTO `login_log` VALUES (77, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 14:54:40.182676');
INSERT INTO `login_log` VALUES (78, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 14:57:28.190513');
INSERT INTO `login_log` VALUES (79, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 15:04:09.424963');
INSERT INTO `login_log` VALUES (80, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 15:10:12.170606');
INSERT INTO `login_log` VALUES (81, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 15:10:17.629855');
INSERT INTO `login_log` VALUES (82, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 15:26:30.458812');
INSERT INTO `login_log` VALUES (83, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 15:59:36.100854');
INSERT INTO `login_log` VALUES (84, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 16:00:08.485351');
INSERT INTO `login_log` VALUES (85, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 16:09:10.121028');
INSERT INTO `login_log` VALUES (86, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 16:13:42.179144');
INSERT INTO `login_log` VALUES (87, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 16:17:17.395331');
INSERT INTO `login_log` VALUES (88, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 16:18:25.205854');
INSERT INTO `login_log` VALUES (89, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 16:18:30.752743');
INSERT INTO `login_log` VALUES (90, 'POST', 'admin', '/login', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 16:28:45.459702');
INSERT INTO `login_log` VALUES (91, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 16:30:18.784214');
INSERT INTO `login_log` VALUES (92, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-16 16:30:22.750186');
INSERT INTO `login_log` VALUES (93, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 09:53:42.747303');
INSERT INTO `login_log` VALUES (94, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 09:57:01.249030');
INSERT INTO `login_log` VALUES (95, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 09:57:06.434770');
INSERT INTO `login_log` VALUES (96, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 09:57:50.986362');
INSERT INTO `login_log` VALUES (97, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 09:59:14.968186');
INSERT INTO `login_log` VALUES (98, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 10:08:49.575132');
INSERT INTO `login_log` VALUES (99, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 10:12:08.773541');
INSERT INTO `login_log` VALUES (100, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 10:15:14.571943');
INSERT INTO `login_log` VALUES (101, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 10:15:26.037442');
INSERT INTO `login_log` VALUES (102, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 10:21:46.114393');
INSERT INTO `login_log` VALUES (103, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 10:23:28.395670');
INSERT INTO `login_log` VALUES (104, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 10:29:25.637413');
INSERT INTO `login_log` VALUES (105, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 10:33:18.066327');
INSERT INTO `login_log` VALUES (106, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 10:34:47.508207');
INSERT INTO `login_log` VALUES (107, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 10:34:47.992973');
INSERT INTO `login_log` VALUES (108, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 11:04:21.337373');
INSERT INTO `login_log` VALUES (109, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 11:12:36.882232');
INSERT INTO `login_log` VALUES (110, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 11:12:42.472565');
INSERT INTO `login_log` VALUES (111, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 11:14:03.913236');
INSERT INTO `login_log` VALUES (112, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 11:14:16.953892');
INSERT INTO `login_log` VALUES (113, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 11:17:41.695658');
INSERT INTO `login_log` VALUES (114, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 11:17:46.931857');
INSERT INTO `login_log` VALUES (115, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 12:11:23.607564');
INSERT INTO `login_log` VALUES (116, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 12:15:04.077278');
INSERT INTO `login_log` VALUES (117, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 12:18:03.982498');
INSERT INTO `login_log` VALUES (118, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 12:19:27.540998');
INSERT INTO `login_log` VALUES (119, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-17 12:21:59.713651');
INSERT INTO `login_log` VALUES (120, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-18 08:14:38.168415');
INSERT INTO `login_log` VALUES (121, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-18 08:16:26.910840');
INSERT INTO `login_log` VALUES (122, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-18 08:23:55.087354');
INSERT INTO `login_log` VALUES (123, 'POST', '100401', '/login/', '密码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0', '2023-09-18 08:24:42.040395');
INSERT INTO `login_log` VALUES (124, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0', '2023-09-18 08:24:53.915150');
INSERT INTO `login_log` VALUES (125, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0', '2023-09-18 08:24:59.738634');
INSERT INTO `login_log` VALUES (126, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-18 08:26:41.010866');
INSERT INTO `login_log` VALUES (127, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-18 08:26:48.137661');
INSERT INTO `login_log` VALUES (128, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-18 08:28:38.468253');
INSERT INTO `login_log` VALUES (129, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-18 08:30:43.426536');
INSERT INTO `login_log` VALUES (130, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-18 08:30:48.875161');
INSERT INTO `login_log` VALUES (131, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-18 08:33:39.442700');
INSERT INTO `login_log` VALUES (132, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-18 08:35:35.077687');
INSERT INTO `login_log` VALUES (133, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2023-09-18 08:39:43.762702');
INSERT INTO `login_log` VALUES (235, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:07:39.091446');
INSERT INTO `login_log` VALUES (236, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:09:20.794603');
INSERT INTO `login_log` VALUES (237, 'POST', 'ceshi', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:09:26.937976');
INSERT INTO `login_log` VALUES (238, 'GET', 'ceshi', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:10:00.100083');
INSERT INTO `login_log` VALUES (239, 'POST', 'ceshi', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:10:11.289098');
INSERT INTO `login_log` VALUES (240, 'POST', 'ceshi', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:10:15.002499');
INSERT INTO `login_log` VALUES (241, 'GET', 'ceshi', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:10:20.051283');
INSERT INTO `login_log` VALUES (242, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:10:25.953109');
INSERT INTO `login_log` VALUES (243, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:10:56.440159');
INSERT INTO `login_log` VALUES (244, 'POST', 'ceshi', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:11:03.297243');
INSERT INTO `login_log` VALUES (245, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:40:06.388422');
INSERT INTO `login_log` VALUES (246, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:40:23.101033');
INSERT INTO `login_log` VALUES (247, 'POST', 'ceshi', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:44:57.418160');
INSERT INTO `login_log` VALUES (248, 'GET', 'ceshi', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:45:56.990964');
INSERT INTO `login_log` VALUES (249, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:46:05.233980');
INSERT INTO `login_log` VALUES (250, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:55:26.023766');
INSERT INTO `login_log` VALUES (251, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:56:57.262929');
INSERT INTO `login_log` VALUES (252, 'POST', 'ceshi', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 16:57:07.636707');
INSERT INTO `login_log` VALUES (253, 'GET', 'ceshi', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 17:00:58.547063');
INSERT INTO `login_log` VALUES (254, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 17:01:04.473209');
INSERT INTO `login_log` VALUES (255, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 17:11:06.924370');
INSERT INTO `login_log` VALUES (256, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 17:11:11.049690');
INSERT INTO `login_log` VALUES (257, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 17:22:17.527282');
INSERT INTO `login_log` VALUES (258, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:03:39.528480');
INSERT INTO `login_log` VALUES (259, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:31:00.594779');
INSERT INTO `login_log` VALUES (260, 'POST', 'ceshi', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:33:17.844410');
INSERT INTO `login_log` VALUES (261, 'GET', 'ceshi', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:34:43.775529');
INSERT INTO `login_log` VALUES (262, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:34:50.131768');
INSERT INTO `login_log` VALUES (263, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:34:53.880460');
INSERT INTO `login_log` VALUES (264, 'GET', 'admin', '/login-out', '退出登录', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:35:24.471216');
INSERT INTO `login_log` VALUES (265, 'POST', 'ceshi', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-12 20:35:30.602513');
INSERT INTO `login_log` VALUES (266, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-13 20:32:53.136410');
INSERT INTO `login_log` VALUES (267, 'POST', 'admin', '/login/', '验证码错误', '127.0.0.1', 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-13 20:32:56.413902');
INSERT INTO `login_log` VALUES (268, 'POST', 'admin', '/login/', '登陆成功', '127.0.0.1', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/124.0.0.0', '2024-05-13 20:32:59.994877');

-- ----------------------------
-- Table structure for role_power
-- ----------------------------
DROP TABLE IF EXISTS `role_power`;
CREATE TABLE `role_power`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `power_id` bigint NULL DEFAULT NULL,
  `power_type_id` bigint NULL DEFAULT NULL,
  `role_id` bigint NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_power_power_id_b1462a48_fk_sys_power_id`(`power_id` ASC) USING BTREE,
  INDEX `role_power_power_type_id_ea8fb8ef_fk_power_type_id`(`power_type_id` ASC) USING BTREE,
  INDEX `role_power_role_id_0f02464a_fk_sys_role_id`(`role_id` ASC) USING BTREE,
  CONSTRAINT `role_power_power_id_b1462a48_fk_sys_power_id` FOREIGN KEY (`power_id`) REFERENCES `sys_power` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `role_power_power_type_id_ea8fb8ef_fk_power_type_id` FOREIGN KEY (`power_type_id`) REFERENCES `sys_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `role_power_role_id_0f02464a_fk_sys_role_id` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 556 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role_power
-- ----------------------------
INSERT INTO `role_power` VALUES (489, 1, 1, 1);
INSERT INTO `role_power` VALUES (490, 2, 1, 1);
INSERT INTO `role_power` VALUES (491, 54, 1, 1);
INSERT INTO `role_power` VALUES (492, 3, 2, 1);
INSERT INTO `role_power` VALUES (493, 4, 2, 1);
INSERT INTO `role_power` VALUES (494, 5, 2, 1);
INSERT INTO `role_power` VALUES (495, 6, 2, 1);
INSERT INTO `role_power` VALUES (496, 64, 2, 1);
INSERT INTO `role_power` VALUES (497, 55, 2, 1);
INSERT INTO `role_power` VALUES (498, 25, 2, 1);
INSERT INTO `role_power` VALUES (499, 26, 2, 1);
INSERT INTO `role_power` VALUES (500, 7, 3, 1);
INSERT INTO `role_power` VALUES (501, 8, 3, 1);
INSERT INTO `role_power` VALUES (502, 9, 3, 1);
INSERT INTO `role_power` VALUES (503, 10, 3, 1);
INSERT INTO `role_power` VALUES (504, 11, 3, 1);
INSERT INTO `role_power` VALUES (505, 38, 3, 1);
INSERT INTO `role_power` VALUES (506, 12, 3, 1);
INSERT INTO `role_power` VALUES (507, 13, 3, 1);
INSERT INTO `role_power` VALUES (508, 14, 3, 1);
INSERT INTO `role_power` VALUES (509, 15, 3, 1);
INSERT INTO `role_power` VALUES (510, 16, 3, 1);
INSERT INTO `role_power` VALUES (511, 17, 3, 1);
INSERT INTO `role_power` VALUES (512, 18, 3, 1);
INSERT INTO `role_power` VALUES (513, 19, 3, 1);
INSERT INTO `role_power` VALUES (514, 20, 3, 1);
INSERT INTO `role_power` VALUES (515, 21, 3, 1);
INSERT INTO `role_power` VALUES (516, 22, 3, 1);
INSERT INTO `role_power` VALUES (517, 23, 3, 1);
INSERT INTO `role_power` VALUES (518, 24, 3, 1);
INSERT INTO `role_power` VALUES (519, 65, 3, 1);
INSERT INTO `role_power` VALUES (520, 66, 3, 1);
INSERT INTO `role_power` VALUES (521, 59, 3, 1);
INSERT INTO `role_power` VALUES (522, 60, 3, 1);
INSERT INTO `role_power` VALUES (523, 61, 3, 1);
INSERT INTO `role_power` VALUES (524, 58, 3, 1);
INSERT INTO `role_power` VALUES (525, 27, 3, 1);
INSERT INTO `role_power` VALUES (526, 28, 3, 1);
INSERT INTO `role_power` VALUES (545, 67, 1, 1);
INSERT INTO `role_power` VALUES (546, 67, 1, 6);
INSERT INTO `role_power` VALUES (547, 54, 1, 6);
INSERT INTO `role_power` VALUES (548, 55, 2, 6);
INSERT INTO `role_power` VALUES (549, 64, 2, 6);
INSERT INTO `role_power` VALUES (550, 59, 3, 6);
INSERT INTO `role_power` VALUES (551, 60, 3, 6);
INSERT INTO `role_power` VALUES (552, 61, 3, 6);
INSERT INTO `role_power` VALUES (553, 58, 3, 6);
INSERT INTO `role_power` VALUES (554, 65, 3, 6);
INSERT INTO `role_power` VALUES (555, 66, 3, 6);

-- ----------------------------
-- Table structure for sys_department
-- ----------------------------
DROP TABLE IF EXISTS `sys_department`;
CREATE TABLE `sys_department`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `leader` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `enable` int NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `details` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sort` int NULL DEFAULT NULL,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_department
-- ----------------------------
INSERT INTO `sys_department` VALUES (1, '研发部', 'zjq', 1, NULL, NULL, NULL, NULL, '2023-09-14 14:31:38.000000', '2023-09-14 15:21:41.407190');
INSERT INTO `sys_department` VALUES (10, '测试部', '测试', 1, NULL, NULL, NULL, NULL, '2024-05-12 16:07:59.025708', '2024-05-12 16:07:59.025708');

-- ----------------------------
-- Table structure for sys_power
-- ----------------------------
DROP TABLE IF EXISTS `sys_power`;
CREATE TABLE `sys_power`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type_id` bigint NULL DEFAULT NULL,
  `code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `url` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `parent_id` int NOT NULL,
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sort` int NULL DEFAULT NULL,
  `enable` int NOT NULL,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_power_type_id_c19e2798`(`type_id` ASC) USING BTREE,
  CONSTRAINT `sys_power_type_id_c19e2798_fk_power_type_id` FOREIGN KEY (`type_id`) REFERENCES `sys_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_power
-- ----------------------------
INSERT INTO `sys_power` VALUES (1, '系统管理', 1, '', NULL, 0, 'fa fa-gears', 1, 1, '2022-08-21 13:40:46.984340', '2022-08-21 13:40:46.984340');
INSERT INTO `sys_power` VALUES (2, '日志管理', 1, '', NULL, 0, 'fa fa-sitemap', 2, 1, '2023-09-13 13:28:52.421054', '2023-09-19 10:35:14.953501');
INSERT INTO `sys_power` VALUES (3, '用户管理', 2, '/user-manage', NULL, 1, 'fa fa-user', 1, 1, '2022-08-21 13:41:42.752289', '2022-08-21 13:41:42.753292');
INSERT INTO `sys_power` VALUES (4, '部门管理', 2, '/department-manage', NULL, 1, 'fa fa-navicon', 2, 1, '2022-08-21 13:42:15.823357', '2024-05-13 20:33:41.945563');
INSERT INTO `sys_power` VALUES (5, '角色管理', 2, '/role-manage', NULL, 1, 'fa fa-user-plus', 3, 1, '2022-08-21 13:42:15.823357', '2022-08-21 13:42:15.823357');
INSERT INTO `sys_power` VALUES (6, '权限管理', 2, '/power-manage', NULL, 1, 'fa fa-key', 4, 1, '2022-08-21 13:42:49.405794', '2022-08-21 13:42:49.405794');
INSERT INTO `sys_power` VALUES (7, '用户新增', 3, 'user:add', NULL, 3, '', 1, 1, '2022-08-21 14:21:40.243014', '2022-08-21 14:21:40.243014');
INSERT INTO `sys_power` VALUES (8, '用户删除', 3, 'user:delete', NULL, 3, '', 2, 1, '2022-08-22 00:02:22.548888', '2022-08-22 00:02:22.548888');
INSERT INTO `sys_power` VALUES (9, '用户编辑', 3, 'user:edit', NULL, 3, '', 3, 1, '2022-08-21 14:21:40.243014', '2022-08-21 14:21:40.243014');
INSERT INTO `sys_power` VALUES (10, '角色更新', 3, 'user:role-update', NULL, 3, '', 4, 1, '2022-08-22 00:03:44.909199', '2022-08-22 00:03:44.909199');
INSERT INTO `sys_power` VALUES (11, '禁用启用', 3, 'user:enable', NULL, 3, '', 5, 1, '2022-08-22 00:15:49.725357', '2022-08-22 00:15:49.725357');
INSERT INTO `sys_power` VALUES (12, '部门新增', 3, 'department:add', NULL, 4, '', 1, 1, '2022-08-21 14:21:40.243014', '2022-08-21 14:21:40.243014');
INSERT INTO `sys_power` VALUES (13, '部门删除', 3, 'department:delete', NULL, 4, '', 2, 1, '2022-08-22 00:02:22.548888', '2022-08-22 00:02:22.548888');
INSERT INTO `sys_power` VALUES (14, '部门编辑', 3, 'department:edit', NULL, 4, '', 3, 1, '2022-08-22 00:03:44.909199', '2022-08-22 00:03:44.909199');
INSERT INTO `sys_power` VALUES (15, '禁用启用', 3, 'department:enable', NULL, 4, '', 4, 1, '2022-08-22 00:15:49.725357', '2022-08-22 00:15:49.725357');
INSERT INTO `sys_power` VALUES (16, '角色新增', 3, 'role:add', NULL, 5, '', 1, 1, '2022-08-22 00:23:52.949521', '2022-08-22 00:23:52.949521');
INSERT INTO `sys_power` VALUES (17, '角色删除', 3, 'role:delete', NULL, 5, '', 2, 1, '2022-08-22 00:24:32.076893', '2022-08-22 00:24:32.076893');
INSERT INTO `sys_power` VALUES (18, '角色编辑', 3, 'role:edit', NULL, 5, '', 3, 1, '2022-08-22 00:25:05.360926', '2022-08-22 00:25:05.361931');
INSERT INTO `sys_power` VALUES (19, '角色权限', 3, 'role:power', NULL, 5, '', 4, 1, '2022-08-22 00:25:05.360926', '2022-08-22 00:25:05.361931');
INSERT INTO `sys_power` VALUES (20, '禁用启用', 3, 'role:enable', NULL, 5, '', 5, 1, '2022-08-22 00:25:31.890194', '2022-08-22 00:25:31.890194');
INSERT INTO `sys_power` VALUES (21, '权限新增', 3, 'power:add', NULL, 6, '', 1, 1, '2022-08-22 00:26:06.919411', '2022-08-22 00:26:06.919411');
INSERT INTO `sys_power` VALUES (22, '权限删除', 3, 'power:delete', NULL, 6, '', 2, 1, '2022-08-22 00:26:37.276269', '2022-08-22 00:26:37.276269');
INSERT INTO `sys_power` VALUES (23, '权限编辑', 3, 'power:edit', NULL, 6, '', 3, 1, '2022-08-22 00:26:37.276269', '2022-08-22 00:26:37.276269');
INSERT INTO `sys_power` VALUES (24, '禁用启用', 3, 'power:enable', NULL, 6, '', 4, 1, '2022-08-22 00:27:07.237696', '2022-08-22 00:27:07.237696');
INSERT INTO `sys_power` VALUES (25, '登陆日志', 2, '/login-log', NULL, 2, 'fa fa-outdent', 1, 1, '2023-09-13 14:13:57.457726', '2023-09-19 10:25:23.570577');
INSERT INTO `sys_power` VALUES (26, '操作日志', 2, '/exec-log', NULL, 2, 'fa fa-indent', 2, 1, '2023-09-13 14:12:44.636779', '2023-09-19 10:25:32.620850');
INSERT INTO `sys_power` VALUES (27, '登陆日志删除', 3, 'login-log:delete', NULL, 25, '', 1, 1, '2022-08-24 11:33:45.484369', '2022-08-24 11:33:45.484369');
INSERT INTO `sys_power` VALUES (28, '操作日志删除', 3, 'exec-log:delete', NULL, 26, '', 1, 1, '2022-08-24 11:33:45.484369', '2022-08-24 11:33:45.484369');
INSERT INTO `sys_power` VALUES (38, '重置密码', 2, 'user:reset-password', NULL, 3, NULL, 6, 1, '2023-09-15 16:24:38.406954', '2023-09-15 16:24:58.057427');
INSERT INTO `sys_power` VALUES (54, '商品管理', 1, NULL, NULL, 0, NULL, 1, 1, '2024-05-08 22:56:45.333217', '2024-05-12 20:31:43.690709');
INSERT INTO `sys_power` VALUES (55, '商品信息', 2, '/product_manage_page', NULL, 54, 'fa fa-briefcase', 2, 1, '2024-05-08 23:00:47.565114', '2024-05-12 20:31:50.349885');
INSERT INTO `sys_power` VALUES (58, '商品数据', 3, '/product_manage_list', NULL, 55, NULL, 3, 1, '2024-05-09 09:44:35.134007', '2024-05-09 09:44:49.114199');
INSERT INTO `sys_power` VALUES (59, '增加商品', 2, '/product_manage_add', NULL, 55, NULL, 1, 1, '2024-05-09 09:56:13.890809', '2024-05-09 09:56:26.257808');
INSERT INTO `sys_power` VALUES (60, '编辑商品', 3, '/product_manage_edit', NULL, 55, NULL, 1, 1, '2024-05-09 10:37:03.775200', '2024-05-09 10:37:12.297593');
INSERT INTO `sys_power` VALUES (61, '删除商品', 3, '/product_manage_del', NULL, 55, NULL, 2, 1, '2024-05-09 10:57:31.073496', '2024-05-09 10:57:39.085255');
INSERT INTO `sys_power` VALUES (64, '出库管理', 2, '/shipments_manage_page', NULL, 67, 'fa fa-bolt', 1, 1, '2024-05-09 11:16:49.472139', '2024-05-12 17:15:34.970510');
INSERT INTO `sys_power` VALUES (65, '出库操作', 3, '/shipments_manage_send', NULL, 64, NULL, 1, 1, '2024-05-09 11:24:55.343627', '2024-05-09 11:36:53.542351');
INSERT INTO `sys_power` VALUES (66, '出库数据', 3, '/shipments_manage_list', NULL, 64, NULL, 1, 1, '2024-05-09 11:28:25.793063', '2024-05-09 11:36:50.756029');
INSERT INTO `sys_power` VALUES (67, '出库管理', 1, NULL, NULL, 0, NULL, 1, 1, '2024-05-12 17:15:21.621411', '2024-05-12 17:15:21.621411');
INSERT INTO `sys_power` VALUES (67, '显示条码', 3, '/product_manage_code', NULL, 55, NULL, 1, 1, '2024-05-13 09:41:40.848462', '2024-05-13 09:42:18.438194');
INSERT INTO `sys_power` VALUES (68, '商品图片', 3, '/product_manage_img', NULL, 55, NULL, 1, 1, '2024-05-14 10:57:14.212423', '2024-05-14 10:57:39.937764');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `enable` int NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `details` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sort` int NULL DEFAULT NULL,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'admin', 1, '系统所有权限', NULL, NULL, '2022-08-16 23:14:27.974460', '2022-08-16 23:14:27.974460');
INSERT INTO `sys_role` VALUES (6, '测试', '测试', 1, NULL, NULL, NULL, '2024-05-12 16:08:25.589710', '2024-05-12 16:08:25.589710');

-- ----------------------------
-- Table structure for sys_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_type`;
CREATE TABLE `sys_type`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `power_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_type
-- ----------------------------
INSERT INTO `sys_type` VALUES (1, '目录');
INSERT INTO `sys_type` VALUES (2, '菜单');
INSERT INTO `sys_type` VALUES (3, '按钮');
INSERT INTO `sys_type` VALUES (4, '其他');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `id_number` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `id_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `enable` int NOT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(6) NOT NULL,
  `department_id` bigint NULL DEFAULT NULL,
  `role_id` bigint NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `sys_user_id_number_225e686e_uniq`(`id_number` ASC) USING BTREE,
  INDEX `sys_user_department_id_32761663_fk_sys_department_id`(`department_id` ASC) USING BTREE,
  INDEX `sys_user_role_id_8a2e10d7_fk_sys_role_id`(`role_id` ASC) USING BTREE,
  CONSTRAINT `sys_user_department_id_32761663_fk_sys_department_id` FOREIGN KEY (`department_id`) REFERENCES `sys_department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sys_user_role_id_8a2e10d7_fk_sys_role_id` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', 'pbkdf2_sha256$390000$CQPGUE8Q56VJgZFF1TgbgO$gosbmAx9RhF9MyVbLQbLPiyoWRK7+ChAaa1DcyIZHEE=', 'admin', '管理员', 1, '', '2023-09-14 11:07:14.000000', 1, 1);
INSERT INTO `sys_user` VALUES (13, 'ceshi', 'pbkdf2_sha256$390000$uqPMR3z7QFceAgGUDSimlY$XJha2sXSptbK8aa0OwqIVJl2S6r6rMwfgeZJTnsb9oQ=', '测试', '', 1, '', '2024-05-12 16:08:15.782931', 10, 6);

-- ----------------------------
-- Table structure for t_product
-- ----------------------------
DROP TABLE IF EXISTS `t_product`;
CREATE TABLE `t_product`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `specification` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `price` double NOT NULL,
  `quantity` bigint NOT NULL,
  `classification` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `manage_unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `person` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_year` bigint NOT NULL,
  `stay_year` bigint NOT NULL,
  `quality_grade` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `other` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `fk_department_id` bigint NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `t_product_fk_department_id_86ef1201_fk_sys_department_id`(`fk_department_id` ASC) USING BTREE,
  CONSTRAINT `t_product_fk_department_id_86ef1201_fk_sys_department_id` FOREIGN KEY (`fk_department_id`) REFERENCES `sys_department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_product
-- ----------------------------
INSERT INTO `t_product` VALUES (1, '苹果手机', '14pro', 999, 10, '手机', '手机', '手机', '个', 0, 1, '新品', '1', 1);
INSERT INTO `t_product` VALUES (2, '华为手机', 'mate60', 0, 1, '手机', '手机', '手机', '个', 0, 1, '新品', '1', 10);
INSERT INTO `t_product` VALUES (3, '苹果14', '1', 1, 100, '1', '1', '1', '1', 0, 1, '1', '1', 10);

-- ----------------------------
-- Table structure for t_shipments
-- ----------------------------
DROP TABLE IF EXISTS `t_shipments`;
CREATE TABLE `t_shipments`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `quantity` bigint NOT NULL,
  `fk_department_id` bigint NULL DEFAULT NULL,
  `fk_product_id` bigint NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `t_shipments_fk_department_id_25fddfea_fk_sys_department_id`(`fk_department_id` ASC) USING BTREE,
  INDEX `t_shipments_fk_product_id_b6c39223_fk_t_product_id`(`fk_product_id` ASC) USING BTREE,
  CONSTRAINT `t_shipments_fk_department_id_25fddfea_fk_sys_department_id` FOREIGN KEY (`fk_department_id`) REFERENCES `sys_department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `t_shipments_fk_product_id_b6c39223_fk_t_product_id` FOREIGN KEY (`fk_product_id`) REFERENCES `t_product` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_shipments
-- ----------------------------
INSERT INTO `t_shipments` VALUES (1, 1, 10, 3);

-- ----------------------------
-- Table structure for web_logo
-- ----------------------------
DROP TABLE IF EXISTS `web_logo`;
CREATE TABLE `web_logo`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `desc` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` int NULL DEFAULT NULL,
  `url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `parent_id` int NULL DEFAULT NULL,
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sort` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of web_logo
-- ----------------------------
INSERT INTO `web_logo` VALUES (1, '首页', 'homeInfo-title', 0, '/home', 1, NULL, 1);
INSERT INTO `web_logo` VALUES (2, '仓库管理系统', 'logoInfo-title', 1, '/logo', 2, '/static/images/logo.png', 1);
INSERT INTO `web_logo` VALUES (3, '仓库管理系统', 'menuInfo-title', 2, '', 3, 'fa fa-address-book', 1);

DROP TABLE IF EXISTS `t_product_upload_image`;
CREATE TABLE `t_product_upload_image`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` datetime(6) NOT NULL,
  `fk_product_id` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `t_product_upload_image_fk_product_id_146fa1a2_fk_t_product_id`(`fk_product_id`) USING BTREE,
  CONSTRAINT `t_product_upload_image_fk_product_id_146fa1a2_fk_t_product_id` FOREIGN KEY (`fk_product_id`) REFERENCES `t_product` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
