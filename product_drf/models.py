from django.db import models


# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=64, null=False, blank=False, default='', verbose_name='名字')
    specification = models.CharField(max_length=128, null=False, blank=False, default='', verbose_name='规格')
    price = models.FloatField(null=False, blank=False, default=0, verbose_name='价格')
    quantity = models.BigIntegerField(null=False, blank=False, default=0, verbose_name='数量')
    classification = models.CharField(max_length=255, null=False, blank=False, default='', verbose_name='分类')
    manage_unit = models.CharField(max_length=255, null=False, blank=False, default='', verbose_name='管理单位')
    person = models.CharField(max_length=255, null=False, blank=False, default='', verbose_name='责任人')
    unit = models.CharField(max_length=255, null=False, blank=False, default='', verbose_name='计量单位')
    user_year = models.BigIntegerField(null=False, blank=False, default=0, verbose_name='使用期限（月）')
    stay_year = models.BigIntegerField(null=False, blank=False, default=0, verbose_name='存储期限（月）')
    quality_grade = models.CharField(max_length=255, null=False, blank=False, default='', verbose_name='质量等级')
    other = models.CharField(max_length=255, null=False, blank=False, default='', verbose_name='备注')
    # barcode_image = models.ImageField(upload_to='barcodes/', blank=True, null=True, verbose_name='条形码')
    fk_department = models.ForeignKey(to='system_manage.Department',
                                      to_field='id',
                                      related_name='fk_department_to_product_set',
                                      related_query_name='fk_department_to_product_set',
                                      blank=False, null=True, verbose_name='库房表的外键',
                                      on_delete=models.CASCADE)

    class Meta:
        db_table = "t_product"
        verbose_name = '产品管理'
        verbose_name_plural = verbose_name


class ProductUploadImage(models.Model):
    fk_product = models.ForeignKey(to='product_drf.Product',
                                   to_field='id',
                                   related_name='fk_product_to_product_upload_image_set',
                                   related_query_name='fk_product_to_product_upload_image_set',
                                   blank=False, null=True, verbose_name='上传图片外键',
                                   on_delete=models.CASCADE)

    filename = models.CharField(max_length=255, null=False, blank=False, default='', verbose_name='上传文件名')
    create_time = models.DateTimeField('创建时间', auto_now_add=True)  # 创建时间

    class Meta:
        db_table = "t_product_upload_image"
        verbose_name = '产品图片记录表'
        verbose_name_plural = verbose_name


class Shipments(models.Model):
    fk_product = models.ForeignKey(to='product_drf.Product',
                                   to_field='id',
                                   related_name='fk_product_to_shipments_set',
                                   related_query_name='fk_product_to_shipments_set',
                                   blank=False, null=True, verbose_name='商品表的外键',
                                   on_delete=models.CASCADE)
    quantity = models.BigIntegerField(null=False, blank=False, default=0, verbose_name='数量')

    fk_department = models.ForeignKey(to='system_manage.Department',
                                      to_field='id',
                                      related_name='fk_department_to_shipments_set',
                                      related_query_name='fk_department_to_shipments_set',
                                      blank=False, null=True, verbose_name='库房表的外键',
                                      on_delete=models.CASCADE)

    class Meta:
        db_table = "t_shipments"
        verbose_name = '出库管理'
        verbose_name_plural = verbose_name


class PutStorage(models.Model):
    fk_product = models.ForeignKey(to='product_drf.Product',
                                   to_field='id',
                                   related_name='fk_product_to_put_storage_set',
                                   related_query_name='fk_product_to_put_storage_set',
                                   blank=False, null=True, verbose_name='商品表的外键',
                                   on_delete=models.CASCADE)
    quantity = models.BigIntegerField(null=False, blank=False, default=0, verbose_name='入库的数量')

    fk_department = models.ForeignKey(to='system_manage.Department',
                                      to_field='id',
                                      related_name='fk_department_to_put_storage_set',
                                      related_query_name='fk_department_to_put_storage_set',
                                      blank=False, null=True, verbose_name='库房表的外键',
                                      on_delete=models.CASCADE)

    class Meta:
        db_table = "t_put_storage"
        verbose_name = '入库管理'
        verbose_name_plural = verbose_name
