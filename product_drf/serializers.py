#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2024/5/11
# @Author  : suk
# @File    : serializers.py
# @Software: PyCharm
from rest_framework import serializers

from product_drf.models import Product, Shipments, ProductUploadImage, PutStorage


class ProductUploadImagelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductUploadImage
        fields = ['id', 'fk_product','filename']


class ProductQueryModelSerializer(serializers.ModelSerializer):
    department_name = serializers.CharField(source='fk_department.name', read_only=True)
    fk_product_to_product_upload_image_set = ProductUploadImagelSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = '__all__'


class ShipmentsQueryModelSerializer(serializers.ModelSerializer):
    department_name = serializers.CharField(source='fk_department.name', read_only=True)
    name = serializers.CharField(source='fk_product.name', read_only=True)
    product_quantity = serializers.CharField(source='fk_product.quantity', read_only=True)

    class Meta:
        model = Shipments
        fields = '__all__'


class ProductModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['quantity']


class ShipmentsUpdateModelSerializer(serializers.ModelSerializer):
    fk_product_to_shipments_set = ProductModelSerializer(many=True, read_only=False)

    class Meta:
        model = Shipments
        fields = ['id', 'quantity', 'fk_product', 'fk_product_id', 'fk_product_to_shipments_set', 'fk_department']

    def create(self, validated_data):
        fk_product_to_shipments_set_data = validated_data.pop('fk_product_to_shipments_set')
        shipment = Shipments.objects.create(**validated_data)
        for fk_product_item in fk_product_to_shipments_set_data:
            fk_product = validated_data.get('fk_product', None)
            fk_product_id = None
            if fk_product:
                fk_product_id = fk_product.pk
            Product.objects.filter(pk=fk_product_id).update(**fk_product_item)
        return shipment


class PutStorageQueryModelSerializer(serializers.ModelSerializer):
    department_name = serializers.CharField(source='fk_department.name', read_only=True)
    name = serializers.CharField(source='fk_product.name', read_only=True)
    product_quantity = serializers.CharField(source='fk_product.quantity', read_only=True)

    class Meta:
        model = PutStorage
        fields = '__all__'

class PutStorageUpdateModelSerializer(serializers.ModelSerializer):
    fk_product_to_put_storage_set = ProductModelSerializer(many=True, read_only=False)

    class Meta:
        model = Shipments
        fields = ['id', 'quantity', 'fk_product', 'fk_product_id', 'fk_product_to_put_storage_set', 'fk_department']

    def create(self, validated_data):
        fk_product_to_put_storage_set_data = validated_data.pop('fk_product_to_put_storage_set')
        put_storage = PutStorage.objects.create(**validated_data)
        for fk_product_item in fk_product_to_put_storage_set_data:
            fk_product = validated_data.get('fk_product', None)
            fk_product_id = None
            if fk_product:
                fk_product_id = fk_product.pk
            Product.objects.filter(pk=fk_product_id).update(**fk_product_item)
        return put_storage
