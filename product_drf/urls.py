# encoding:utf-8
"""
@file = urls
@author = zouju
@create_time = 2023-09-12- 15:07
"""
from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from rest_framework import permissions, routers

from product_drf.views.product import ProductPage, ProductAdd, ProductEdit, ProductGenericView, ProductCode, \
    ConsoleSignImage
from product_drf.views.put_storage import PutStoragePage, PutStorageAdd, PutStorageGenericView
from product_drf.views.shipments import ShipmentsPage, ShipmentsSend, ShipmentGenericView

# # 文档视图
schema_view = get_schema_view(
    # API 信息
    openapi.Info(
        title='DRF API',  # API文档标题
        default_version='V1',  # 版本信息
        description='API 接口文档',  # 描述内容
        terms_of_service='https://test.com',  # 开发团队地址
        contact=openapi.Contact(email='https://test.@163com', url='https://test.com'),  # 联系人信息：邮件、网址
        license=openapi.License(name='Test License'),  # 证书
    ),
    public=True,  # 是否公开
    authentication_classes=[],
    permission_classes=[permissions.AllowAny]  # 设置用户权限

)

router = routers.DefaultRouter()
# get,post, put, delete(单个及多个), enable视图
router.register(r'product_manage_list', ProductGenericView)
router.register(r'shipments_manage_list', ShipmentGenericView)
router.register(r'put_storage_manage_list', PutStorageGenericView)

urlpatterns = [
    # 产品管理
    path('product_manage_page', ProductPage.as_view()),
    path('product_manage_add', ProductAdd.as_view()),
    path('product_manage_edit', ProductEdit.as_view()),
    path('product_manage_code', ProductCode.as_view()),
    path('product_manage_img', ConsoleSignImage.as_view()),

    # 出库管理
    path('shipments_manage_page', ShipmentsPage.as_view()),
    path('shipments_manage_send', ShipmentsSend.as_view()),

    # 入库管理
    path('put_storage_manage_page', PutStoragePage.as_view()),
    path('put_storage_manage_add', PutStorageAdd.as_view()),
]

urlpatterns += router.urls
